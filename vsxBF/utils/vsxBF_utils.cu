/**
 @file formatters/vsx_utils.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-07-01

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "vsxBF_utils.cuh"

namespace rtbf {

// Find the first GPU that can use texture objects (CC >= 3.0)
void getFirstAvailableGPU(int *gpuID) {
  int nDevices;
  CCE(cudaGetDeviceCount(&nDevices));
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    CCE(cudaGetDeviceProperties(&prop, i));
    if (prop.major >= 3) {
      *gpuID = i;
      break;
    }
  }
}

// Get a requried field from a struct
mxArray *getRequiredField(const mxArray *R, const char *field) {
  if (!mxIsStruct(R)) mexErrMsgTxt("Input must be a structure.");
  mxArray *A = mxGetField(R, 0, field);
  if (A == NULL) {
    char msg[256];
    sprintf(msg, "Required field %s is missing.", field);
    mexErrMsgTxt(msg);
  }
  return A;
}
// Get a required field from a struct with type checking for float
mxArray *getRequiredField(const mxArray *R, const char *field, float **dptr) {
  // First, get the required field
  mxArray *A = getRequiredField(R, field);
  // Additionally, check that the datatype is correct
  if (!mxIsEmpty(A) && mxGetClassID(A) != mxSINGLE_CLASS) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected single.", field);
    mexErrMsgTxt(msg);
  }
  return A;
}
// Get a field from a struct safely with type checking for int
mxArray *getRequiredField(const mxArray *R, const char *field, int **dptr) {
  // First, get the required field
  mxArray *A = getRequiredField(R, field);
  // Additionally, check that the datatype is correct
  if (!mxIsEmpty(A) && mxGetClassID(A) != mxINT32_CLASS) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected int32.", field);
    mexErrMsgTxt(msg);
  }
  return A;
}

// Get scalar from a structure
template <typename T>
void getReqScalar(const mxArray *R, T *val, const char *field) {
  mxArray *A = getRequiredField(R, field);
  *val = (T)mxGetScalar(A);
}

// Get optional scalar from a structure if it is a valid field. Do nothing
// otherwise.
template <typename T>
void getOptScalar(const mxArray *R, T *val, const char *field) {
  if (!mxIsStruct(R)) mexErrMsgTxt("Input must be a structure.");
  mxArray *A = mxGetField(R, 0, field);
  if (A != NULL) *val = (T)mxGetScalar(A);
}

// Explicitly instantiate getReqScalar and getOptScalar function
// templates
template void getReqScalar<>(const mxArray *, short *, const char *);
template void getReqScalar<>(const mxArray *, int *, const char *);
template void getReqScalar<>(const mxArray *, float *, const char *);
template void getReqScalar<>(const mxArray *, double *, const char *);
template void getOptScalar<>(const mxArray *, short *, const char *);
template void getOptScalar<>(const mxArray *, int *, const char *);
template void getOptScalar<>(const mxArray *, float *, const char *);
template void getOptScalar<>(const mxArray *, double *, const char *);

// Safely get delay and apodization tables with type and size error checking
void getDelayApodTables(const mxArray *R, float **table, const char *field,
                        int nrows, int ncols, int npages) {
  // Get field with existing checking and type checking
  mxArray *A = getRequiredField(R, field, table);
  // If the table is not specified, return nullptr
  if (mxIsEmpty(A)) {
    *table = nullptr;
    return;
  }
  // Check dimensions
  const mwSize *dims = mxGetDimensions(A);
  int d[3] = {1, 1, 1};
  for (int i = 0; i < mxGetNumberOfDimensions(A); i++) d[i] = dims[i];
  if (nrows != d[0] || ncols != d[1] || npages != d[2]) {
    char msg[256];
    sprintf(msg,
            "%s does not match input dimensions of (%d,%d,%d). Found (% d, % "
            "d, % d).",
            field, nrows, ncols, npages, d[0], d[1], d[2]);
    mexErrMsgTxt(msg);
  }
  *table = (float *)mxGetData(A);
}

// Safely get channel mapping with type and size error checking
void getChannelMapping(const mxArray *R, int **cmap, const char *field,
                       int nchans, int nxmits, int nelems) {
  // If user does not provide channel mapping, assume none
  if (mxIsStruct(R)) {
    mxArray *A = mxGetField(R, 0, field);
    if (A == NULL || mxIsEmpty(A)) {
      if (nchans != nelems)
        mexErrMsgTxt(
            "No channel mapping is provided, but the number of acquisition "
            "channels does not match the number of total channels.");
      else {
        *cmap = nullptr;
        return;
      }
    }
  }

  // Otherwise, get channel mapping safely with exist and type checking
  mxArray *A = getRequiredField(R, field, cmap);
  // Check dimensions
  int N = mxGetNumberOfElements(A);
  if (N != nchans * nxmits) {
    mexErrMsgTxt("The channel mapping must be nchans-by-nxmits.");
  }
  int *map = (int *)mxGetData(A);
  for (int i = 0; i < N; i++) {
    if (map[i] >= nelems) {
      mexErrMsgTxt(
          "Channel mapping entry exceeds the actual number of channels.");
    }
  }

  // If still valid, return pointer to channel mapping.
  *cmap = (int *)mxGetData(A);
}

// Get VSXSampleMode enum value
void getVSXSampleMode(const mxArray *R, VSXSampleMode *mode, float *spcy,
                      const char *field) {
  mxArray *A = getRequiredField(R, field);
  if (!mxIsChar(A)) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected a string.", field);
    mexErrMsgTxt(msg);
  }
  char *str = mxArrayToString(A);
  if (strcmp(str, "BS50BW") == 0) {
    *mode = VSXSampleMode::BS50BW;
    *spcy = 1.0f;  // Every two samples is the I and Q of one sample
    // TODO: Figure out why *spcy = 1.0f and not 0.5f for BS50BW
  } else if (strcmp(str, "BS100BW") == 0) {
    *mode = VSXSampleMode::BS100BW;
    *spcy = 1.0f;  // Every two samples is the I and Q of one sample
  } else if (strcmp(str, "NS200BW") == 0) {
    *mode = VSXSampleMode::NS200BW;
    *spcy = 2.0f;  // Every two samples is the I and Q of one sample
  } else if (strcmp(str, "CUSTOM") == 0) {
    *mode = VSXSampleMode::CUSTOM;
    *spcy = 0.f;
  } else {
    char msg[256];
    sprintf(msg,
            "Unrecognized value for %s. Valid values are 'BS50BW', 'BS100BW', "
            "'NS200BW', or 'CUSTOM'.",
            field);
    mexErrMsgTxt(msg);
  }
}

// Get the SynthesisMode enum value
void getSynthesisMode(const mxArray *R, SynthesisMode *mode,
                      const char *field) {
  // By default, synthesize the transmit aperture
  *mode = SynthesisMode::SynthTx;
  // If specified, synthesize the receive aperture
  if (mxIsStruct(R)) {
    mxArray *A = mxGetField(R, 0, field);
    if (A != NULL && (int)mxGetScalar(A) == 1) {
      *mode = SynthesisMode::SynthRx;
    }
  }
}

// Get a filename as a C-string
void getFilename(const mxArray *R, char *str, const char *field) {
  // If specified, synthesize the receive aperture
  if (mxIsStruct(R)) {
    mxArray *A = getRequiredField(R, field);
    if (!mxIsChar(A)) {
      char msg[256];
      sprintf(msg, "%s is the wrong datatype. Expected a string.", field);
      mexErrMsgTxt(msg);
    }
    char *Astr = mxArrayToString(A);
    // Check that file exists
    if (!std::ifstream(Astr).good()) {
      char msg[256];
      sprintf(msg, "File %s cannot be accessed. Check %s.\n", Astr, field);
      mexErrMsgTxt(msg);
    }
    // Copy string
    strcpy(str, Astr);
  }
}

// Copy output of DataProcessor to MATLAB array
// NOTE: The uninterleave is slow because of memory allocation
void copyToMATLABArrayAsync(DataProcessor<float2> *D, mxArray **A,
                            cudaStream_t stream, bool uninterleave) {
  mwSize nrows = D->getOutputDataArray()->getNx();
  mwSize ncols = D->getOutputDataArray()->getNy();
  mwSize nchans = D->getOutputDataArray()->getNc();
  mwSize nframes = D->getOutputDataArray()->getNf();
  mwSize ndim = 4;
  if (uninterleave) {
    const mwSize dims[] = {nrows, ncols, nchans, nframes};
    *A = mxCreateNumericArray(ndim, dims, mxSINGLE_CLASS, mxCOMPLEX);
    float *h_outR = (float *)mxGetData(*A);
    float *h_outI = (float *)mxGetImagData(*A);
    float2 *h_tmp =
        (float2 *)malloc(sizeof(float2) * nrows * ncols * nchans * nframes);
    D->getOutputDataArray()->copyFromGPUAsync(h_tmp, stream);
    for (int f = 0; f < nframes; f++) {
      for (int k = 0; k < nchans; k++) {
        for (int j = 0; j < ncols; j++) {
          for (int i = 0; i < nrows; i++) {
            int idx = i + nrows * (j + ncols * (k + nchans * f));
            h_outR[idx] = h_tmp[idx].x;
            h_outI[idx] = h_tmp[idx].y;
          }
        }
      }
    }
    free(h_tmp);
  } else {
    const mwSize dims[] = {nrows * 2, ncols, nchans, nframes};
    *A = mxCreateNumericArray(ndim, dims, mxSINGLE_CLASS, mxREAL);
    float2 *h_out = (float2 *)mxGetData(*A);
    D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
  }
}
// Copy output of DataProcessor to MATLAB array
// NOTE: The uninterleave is slow because of memory allocation
void copyToMATLABArrayAsync(DataProcessor<short2> *D, mxArray **A,
                            cudaStream_t stream, bool uninterleave) {
  mwSize nrows = D->getOutputDataArray()->getNx();
  mwSize ncols = D->getOutputDataArray()->getNy();
  mwSize nchans = D->getOutputDataArray()->getNc();
  mwSize nframes = D->getOutputDataArray()->getNf();
  mwSize ndim = 4;
  if (uninterleave) {
    const mwSize dims[] = {nrows, ncols, nchans, nframes};
    *A = mxCreateNumericArray(ndim, dims, mxINT16_CLASS, mxCOMPLEX);
    short *h_outR = (short *)mxGetData(*A);
    short *h_outI = (short *)mxGetImagData(*A);
    short2 *h_tmp =
        (short2 *)malloc(sizeof(short2) * nrows * ncols * nchans * nframes);
    D->getOutputDataArray()->copyFromGPUAsync(h_tmp, stream);
    for (int f = 0; f < nframes; f++) {
      for (int k = 0; k < nchans; k++) {
        for (int j = 0; j < ncols; j++) {
          for (int i = 0; i < nrows; i++) {
            int idx = i + nrows * (j + ncols * (k + nchans * f));
            h_outR[idx] = h_tmp[idx].x;
            h_outI[idx] = h_tmp[idx].y;
          }
        }
      }
    }
    free(h_tmp);
  } else {
    const mwSize dims[] = {nrows * 2, ncols, nchans, nframes};
    *A = mxCreateNumericArray(ndim, dims, mxINT16_CLASS, mxREAL);
    short2 *h_out = (short2 *)mxGetData(*A);
    D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
  }
}
// Copy output of DataProcessor to MATLAB array
void copyToMATLABArrayAsync(DataProcessor<float> *D, mxArray **A,
                            cudaStream_t stream) {
  mwSize nrows = D->getOutputDataArray()->getNx();
  mwSize ncols = D->getOutputDataArray()->getNy();
  mwSize nchans = D->getOutputDataArray()->getNc();
  mwSize nframes = D->getOutputDataArray()->getNf();
  mwSize ndim = 4;
  const mwSize dims[] = {nrows, ncols, nchans, nframes};
  *A = mxCreateNumericArray(ndim, dims, mxSINGLE_CLASS, mxREAL);
  float *h_out = (float *)mxGetData(*A);
  D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
}
// Copy output of DataProcessor to MATLAB array
void copyToMATLABArrayAsync(DataProcessor<short> *D, mxArray **A,
                            cudaStream_t stream) {
  mwSize nrows = D->getOutputDataArray()->getNx();
  mwSize ncols = D->getOutputDataArray()->getNy();
  mwSize nchans = D->getOutputDataArray()->getNc();
  mwSize nframes = D->getOutputDataArray()->getNf();
  mwSize ndim = 4;
  const mwSize dims[] = {nrows, ncols, nchans, nframes};
  *A = mxCreateNumericArray(ndim, dims, mxINT16_CLASS, mxREAL);
  short *h_out = (short *)mxGetData(*A);
  D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
}

}  // namespace rtbf
