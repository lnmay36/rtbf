/**
 @file formatters/VSXDataFormatter.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-13

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "VSXDataFormatter.cuh"

namespace rtbf {
namespace kernels = VSXDataFormatterKernels;

template <typename T_in, typename T_out>
VSXDataFormatter<T_in, T_out>::VSXDataFormatter() {
  reset();
}
template <typename T_in, typename T_out>
VSXDataFormatter<T_in, T_out>::~VSXDataFormatter() {
  reset();
}
template <typename T_in, typename T_out>
void VSXDataFormatter<T_in, T_out>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");
  CCE(cudaSetDevice(this->devID));

  // VSXDataFormatter members
  nsamps = 0;
  nxmits = 0;
  nchans = 0;
  nacqch = 0;
  npulses = 0;
  nframes = 0;
  mode = VSXSampleMode::CUSTOM;
  in.reset();
  chanmap.reset();

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "VSXDataFormatter");
}

// Initialization function
template <typename T_in, typename T_out>
void VSXDataFormatter<T_in, T_out>::initialize(
    int numSamples, int numTransmits, int numChannels, int numAcqChannels,
    int numFrames, VSXSampleMode sampleMode, int numPulsesToSum,
    int *h_channelMapping, int deviceID, cudaStream_t cudaStream,
    int verbosity) {
  // If previously initialized, reset
  reset();

  // Populate parameters
  nsamps = numSamples;
  nxmits = numTransmits;
  nchans = numChannels;
  nacqch = numAcqChannels;
  npulses = numPulsesToSum;
  nframes = numFrames;
  mode = sampleMode;
  this->setDeviceID(deviceID);
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  switch (mode) {
    case VSXSampleMode::BS50BW:
      nsamps /= 2;  // Every two samples is the I and Q of one sample
      this->printMsg("BS50BW", 2);
      break;
    case VSXSampleMode::BS100BW:
      nsamps /= 2;  // Every two samples is the I and Q of one sample
      this->printMsg("BS100BW", 2);
      break;
    case VSXSampleMode::NS200BW:
      nsamps /= 2;  // Every two samples is the I and Q of one sample
      this->printMsg("NS200BW", 2);
      break;
    default:
      this->printMsg("CUSTOM", 2);
      break;
  }

  // Initialize data arrays on current GPU
  CCE(cudaSetDevice(this->devID));

  // Initialize input DataArray (use input numSamples)
  in.initialize(numSamples * npulses * nxmits, 1, nacqch, nframes, deviceID,
                "VSXDataFormatter (input)", this->verb);

  // Initialize output DataArray (use output nsamps)
  this->out.initialize(nsamps, nxmits, nchans, nframes, this->devID,
                       "VSXDataFormatter (output)", this->verb);

  // Store the channel mapping as an array on the GPU if it is used
  if (h_channelMapping != nullptr) {
    chanmap.initialize(nacqch * nxmits, 1, 1, 1, this->devID,
                       "VSXDataFormatter (channel map)", this->verb);
    chanmap.copyToGPU(h_channelMapping);
    this->printMsg("Using custom channel mapping.", this->verb);
  }

  this->isInit = true;
}

// This function copies the data from the host to the GPU and selects
// the proper CUDA kernel to execute
template <typename T_in, typename T_out>
void VSXDataFormatter<T_in, T_out>::formatRawVSXData(T_in *h_raw,
                                                     int pitchInSamps) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }

  // Copy data to the input GPU array
  in.copyToGPUAsync(h_raw, this->stream, sizeof(T_in) * pitchInSamps);

  // Get kernel arguments
  int nx = nsamps;                 // Number of samples
  int ny = nxmits;                 // Number of transmission locations
  int nc = nchans;                 // Number of total channels
  int na = nacqch;                 // Number of acquired channels
  int np = npulses;                // Number of pulses to sum
  int ipitch = in.getNp();         // Input data pitch
  int opitch = this->out.getNp();  // Output data pitch
  int *cmap = nullptr;             // Channel mapping (none by default)
  // If a channel map is given, use it.
  if (chanmap.isInitialized()) cmap = chanmap.getDataPtr();

  // Create computation grid
  dim3 B(16, 1, 16);
  dim3 G((nx - 1) / B.x + 1, (ny - 1) / B.y + 1, (na - 1) / B.z + 1);

  // Loop through frames
  for (int frame = 0; frame < in.getNf(); frame++) {
    T_in *idata = in.getFramePtr(frame);          // Input data pointer
    T_out *odata = this->out.getFramePtr(frame);  // Output data pointer
    switch (mode) {
      case VSXSampleMode::BS50BW:
        kernels::format<T_in, T_out, 1><<<G, B, 0, this->stream>>>(
            nx, ny, nc, na, np, idata, ipitch, odata, opitch, cmap);
        break;
      case VSXSampleMode::BS100BW:
        kernels::format<T_in, T_out, 2><<<G, B, 0, this->stream>>>(
            nx, ny, nc, na, np, idata, ipitch, odata, opitch, cmap);
        break;
      case VSXSampleMode::NS200BW:
        kernels::format<T_in, T_out, 4><<<G, B, 0, this->stream>>>(
            nx, ny, nc, na, np, idata, ipitch, odata, opitch, cmap);
        break;
      default:
        kernels::format<T_in, T_out, 0><<<G, B, 0, this->stream>>>(
            nx, ny, nc, na, np, idata, ipitch, odata, opitch, cmap);
    }
  }
}

// Kernel implementations
// Use templates to create separate kernels at compile time
template <typename T_in, typename T_out, int spc>
__global__ void kernels::format(int nsamps, int nxmits, int nchans, int nacqch,
                                int npulses, T_in *idata, int ipitch,
                                T_out *odata, int opitch, int *cmap) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int acqc = blockIdx.z * blockDim.z + threadIdx.z;  // Acquisition channel

  // Only execute kernel if valid
  if (samp < nsamps && xmit < nxmits && acqc < nacqch) {
    int chan;               // Output channel index; destination of current data
    if (cmap != nullptr) {  // If provided, use channel map
      chan = cmap[acqc + nacqch * xmit];
    } else {  // Otherwise, assume channels are [0, ..., nacqch-1]
      chan = acqc;
    }
    // Advance input data pointer to the channel being processed
    idata += ipitch * acqc;
    // Loop through the number of summed pulses (i.e., for harmonic imaging)
    float2 sum = make_float2(0.f, 0.f);
    for (int p = 0; p < npulses; p++) {
      // Grab data according to the VSXSampleMode
      float2 data;
      if (spc == 0) {  // If treating data as modulated
        int iidx = samp + nsamps * (p + npulses * xmit);
        data = make_float2(idata[iidx], 0.f);
      } else {  // If treating data as baseband
        int iidxI = 0 + 2 * (samp + nsamps * (p + npulses * xmit));
        int iidxQ = 1 + 2 * (samp + nsamps * (p + npulses * xmit));
        data = make_float2(idata[iidxI], idata[iidxQ]);
        // Align the quadrature sample using linear interpolation, i.e.
        //   (I0, Q1, I2, Q3, ...) --> (I0, Q0, I2, Q2, ...)
        float qprev;
        if (samp > 0) {
          qprev = idata[iidxQ - 2];
        } else {
          qprev = 0;
        }
        // If 4 samples per wave, every 3rd and 4th sample are negated.
        if (spc == 4) {
          int sign = (1 - 2 * (samp & 0x1));  // Use bitwise arithmetic
          data *= sign;
          qprev *= -sign;
        }
        // Apply linear interpolation with previous sample
        float frac = spc / 8.f;
        data.y = -1.f * (data.y * (1 - frac) + qprev * frac);
      }
      sum += data;
    }
    // Cast sum to correct datatype and store result in odata
    int oidx = samp + opitch * (xmit + nxmits * chan);
    saveData2(odata, oidx, sum);
  }
}

// Explicitly instantiate template for short input and short2, float2 outputs
template class VSXDataFormatter<short, short2>;
template class VSXDataFormatter<short, float2>;
}
