/**
 @file formatters/vsxBF_utils.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-07-01

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef VSX_UTILS_CUH_
#define VSX_UTILS_CUH_

#include <fstream>
#include "FocusSynAp.cuh"
#include "VSXDataFormatter.cuh"
#include "gpuBF.cuh"
extern "C" {
#include "mex.h"
}

namespace rtbf {

// Find the first GPU that can use texture objects (CC >= 3.0)
void getFirstAvailableGPU(int *gpuID);

// Get a field from a struct safely with optional type-checking
mxArray *getRequiredField(const mxArray *R, const char *field);
mxArray *getRequiredField(const mxArray *R, const char *field, float **dptr);
mxArray *getRequiredField(const mxArray *R, const char *field, int **dptr);

// Get a required scalar from a struct safely
template <typename T>
void getReqScalar(const mxArray *R, T *val, const char *field);

// Get an optional scalar from a struct safely if exist, or do nothing otherwise
template <typename T>
void getOptScalar(const mxArray *R, T *val, const char *field);

// Safely get delay and apodization tables with type and size error checking
void getDelayApodTables(const mxArray *R, float **table, const char *field,
                        int nrows, int ncols, int npages);

// Safely get channel mapping with type and size error checking
void getChannelMapping(const mxArray *R, int **cmap, const char *field,
                       int nchans, int nxmits, int nelems);

// Get the VSXSampleMode enum value
void getVSXSampleMode(const mxArray *R, VSXSampleMode *mode, float *spcy,
                      const char *field);

// Get the SynthesisMode enum value
void getSynthesisMode(const mxArray *R, SynthesisMode *mode, const char *field);

// Get a filename as a C-string
void getFilename(const mxArray *R, char *str, const char *field);

// Copy output of DataProcessor to MATLAB array
void copyToMATLABArrayAsync(DataProcessor<float2> *D, mxArray **A,
                            cudaStream_t stream, bool uninterleave = true);
// Copy output of DataProcessor to MATLAB array
void copyToMATLABArrayAsync(DataProcessor<short2> *D, mxArray **A,
                            cudaStream_t stream, bool uninterleave = true);
// Copy output of DataProcessor to MATLAB array
void copyToMATLABArrayAsync(DataProcessor<float> *D, mxArray **A,
                            cudaStream_t stream);
// Copy output of DataProcessor to MATLAB array
void copyToMATLABArrayAsync(DataProcessor<short> *D, mxArray **A,
                            cudaStream_t stream);

}  // namespace rtbf

#endif
