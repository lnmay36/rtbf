% File name SetUpL12_3v_HI_PlnWv_slsc.m:
%
% For harmonic imaging, 3 pulse sequence with + - + phase is used.
% Tx @ 4.5MHz and receive at 8.9MHz
%
% - Always receive using the center 128 elements, 33:160
% - Select a transmit aperture that will maximize the energy delivered
%   to the desired location

clear all

%% Define acquisition parameters
% Hard parameters (cannot modify during acquisition)
P.na = 25;
P.totalAngle = 10;
P.configName = 'L12-3v_PlnWv_Bmode';
P.spw = 4;
% Modifiable parameters (using the GUI)
P.imageBufferSize = 40;
P.rxStartMm = 1; % Receive start depth
P.rxDepthMm = 30; % Receive end depth
P.rxMaxDepthMm = 50;
savedir = './data/';
vsxBFdir = '/data/dhyun/code/rtbf2/vsxBF/';
P.acqFPS = 100;	% frames per second of data being saved
P.acqPRF = 1e6/10000; % 10kHz PRF

%% Define reconstruction parameters
% R will contain all of the parameters that were used at the time of
% acquisition, just as a way to keep notes of what was going on.
R.addNoise = 0;
R.useNxFc = 1;
R.fovWidthMm = 20;
R.outspwl = 3;
R.fnum = 2;
R.maxlag = 10;
R.brange = [-40 0];
R.crange = [0 0.7];
P.TxVoltage = 0;

%% Fill out the rest of P and R (do not modify!!!)
R.initialized = false;
% Use the center 128 elements (aperture #33) for TX/RX
apers = 65 * ones(P.na,1);
% Acquisition parameters
P.np = 1; % Two pulses in harmonic imaging
switch P.spw
	case 4
		sampleMode = 'NS200BW';
	case 2
		sampleMode = 'BS100BW';
	case 1
		sampleMode = 'BS50BW';
	otherwise
		error('Unsupported samples per wavelength.')
end
% Set angles
if P.na == 1
	P.Angles = 0;	P.dtheta = 0;
else
	P.Angles = linspace(-P.totalAngle/2,P.totalAngle/2,P.na) * pi/180;
	P.dtheta = abs(diff(P.Angles(1:2)));
end

% Reconstruction parameters
R.useMedianFilter = 0;	% Do not median filter by default
R.useEnsembleMode = 0;	% Use averaged CC mode by default
R.showHarmonic = false;	% Start off showing fundamental
if P.spw ~= 4, R.useNxFc = 1; end % Can only use Hilbert tranform with fully sampled data

% Flags for the external processing function
updatedRange = false;
quickUpdate = false; % Flag for small updates to mex function


%% Define system parameters.
Resource.Parameters.numTransmit = 128;  % number of transmit channels.
Resource.Parameters.numRcvChannels = 128;  % number of receive channels.
Resource.Parameters.simulateMode = 0;

%% Specify Trans structure array.
Trans.name = 'L12-3v';
Trans.units = 'wavelengths'; % required in Gen3 to prevent default to mm units
Trans.frequency = 8.929;%10.4167;%9.6154;%8.929;   % center frequency for harmonic imaging receive
% note nominal center frequency in computeTrans is 7.813 MHz
Trans = computeTrans(Trans);  % L12-3v transducer is 'known' transducer so we can use computeTrans.
Trans.maxHighVoltage = 20;

%% Define lengths in wavelengths now that we have Trans.frequency
mmToWvln = @(mm, fcMHz) mm*1e-3 * fcMHz*1e6 / 1540;
wvlnToMm = @(wvln, fcMHz) wvln*1540 / (fcMHz*1e6) * 1e3;
P.rxStart = mmToWvln(P.rxStartMm, Trans.frequency);
P.rxDepth = mmToWvln(P.rxDepthMm, Trans.frequency);
R.fovWidth = mmToWvln(R.fovWidthMm, Trans.frequency);
P.rxMaxDepth = mmToWvln(P.rxMaxDepthMm, Trans.frequency);

%% Specify Media object. 'pt1.m' script defines array of point targets.
pt1;
% npts = 10;
% pts_d = mmToWvln( (1:npts)' * 2, Trans.frequency);
% % npts = 1;
% % pts_d = mmToWvln(10, Trans.frequency);
% Media.MP = cat(2, zeros(npts,2), pts_d, ones(npts,1));
% Media.MP(:,1) = mean(Trans.ElementPos(apers(1) + (0:127),1));
Media.function = 'movePoints';

%% Specify Resources.
% Compute the rows per frame (must be a multiple of 128)
rpf = (P.rxMaxDepth+20) * 2 * P.spw * P.np*P.na;
rpf = ceil(rpf / 128) * 128;
% Live imaging buffer
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = rpf;
Resource.RcvBuffer(1).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(1).numFrames = 10; % Only 10 frames needed for live acq
% Save data buffer
Resource.RcvBuffer(2).datatype = 'int16';
Resource.RcvBuffer(2).rowsPerFrame = rpf;
Resource.RcvBuffer(2).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(2).numFrames = P.imageBufferSize;

%% Specify Transmit waveform structure.
% Harmonic pulses
TW(1).type = 'parametric';
TW(1).Parameters = [Trans.frequency/2, 1, 2,  1];   % A, B, C, D
TW(2).type = 'parametric';
TW(2).Parameters = [Trans.frequency/2, 1, 2, -1];   % A, B, C, D

%% Specify TX structure array.
TX = repmat(struct('waveform', 1, ...
				   'Origin', [0, 0, 0], ...
				   'aperture', 1, ...
				   'Apod', ones(1,Resource.Parameters.numTransmit), ...
				   'focus', 0, ...
				   'Steer', [0, 0], ...
				   'Delay', zeros(1,Resource.Parameters.numTransmit)), 1, P.np*P.na);
% - Set event specific TX attributes.
% Harmonic pulses
for n = 1:P.np*P.na
	[p, a] = ind2sub([P.np P.na], n); % p is fast changing dimension
	angle = P.Angles(a);
	TX(n).aperture = apers(a);
	TX(n).Steer(1) = angle;
	TX(n).Delay = computeTXDelays(TX(n));
	if p == 1 % Positive polarity pulse
		TX(n).waveform = 1;
	elseif p == 2 % Negative polarity pulse
		TX(n).waveform = 2;
	end
end

%% Specify TPC structures.
TPC(1).name = 'Imaging';
TPC(1).maxHighVoltage = 20;

%% Specify TGC Waveform structure.
TGC.CntrlPts = [442,599,728,795,914,981,1023,1023];
TGC.rangeMax = P.rxDepth;
TGC.Waveform = computeTGCWaveform(TGC);

%% Specify Receive structure arrays
% Use the filter coefficients generated by the Verasonics tool
% This filter gives an 80% fraction bandwidth, centered around the second
% harmonic frequency of 10.417
if Trans.frequency < 10 || Trans.frequency > 11
	warning('The filter coefficients are tuned to 10.417MHz, but the RX frequency is %.3f.\n',Trans.frequency);
end
lpfcoeff = [+0.00000 +0.00000 +0.00000 +0.00000 +0.00000 +0.00000 ...
			+0.00000 +0.00000 +0.00000 +0.00000 +0.00000 +1.00000];
inpcoeff = [-0.00031 +0.00000 +0.00232 +0.00000 -0.00266 +0.00000 -0.00677 ...
			+0.00000 +0.01743 +0.00000 +0.00137 +0.00000 -0.04916 +0.00000 ...
			+0.04724 +0.00000 +0.08575 -0.00003 -0.29401 +0.00000 +0.39764];

%   endDepth - add additional acquisition depth to account for some channels
%              having longer path lengths.
%   InputFilter - The same coefficients are used for all channels. The
%              coefficients below give a broad bandwidth bandpass filter.
maxAcqLength = sqrt(P.rxDepth^2 + (Trans.numelements*Trans.spacing)^2) - P.rxStart;
wlsPer128 = 128/(P.spw*2); % wavelengths in 128 samples for 4 samplesPerWave
Receive = repmat(struct('Apod', ones(1,128), ...
						'aperture',1, ...
						'startDepth', P.rxStart, ...
						'endDepth', P.rxStart + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
						'TGC', 1, ...
						'bufnum', 1, ...
						'framenum', 1, ...
						'acqNum', 1, ...
						'sampleMode', sampleMode, ...
						'mode', 0, ...
						'LowPassCoef', lpfcoeff, ...
						'InputFilter', inpcoeff, ...
						'callMediaFunc', 0),1, ...
						P.na*P.np*Resource.RcvBuffer(1).numFrames + ...
						P.na*P.np*Resource.RcvBuffer(2).numFrames);
% - Set event specific Receive attributes for each frame.
% Real-time imaging loop
for i = 1:Resource.RcvBuffer(1).numFrames
	frameStartIdx = (i-1) * P.na*P.np;
	Receive(frameStartIdx+1).callMediaFunc = 1;
	
	% Harmonic pulses
	offset = frameStartIdx;
	for n = 1:P.na*P.np
		[p, a] = ind2sub([P.np P.na], n); % p is fast changing dimension
		Receive(offset+n).framenum = i;
		Receive(offset+n).aperture = apers(a);
		Receive(offset+n).acqNum = n;
	end
end

% Data saving loop
for i = 1:Resource.RcvBuffer(2).numFrames
	frameStartIdx = (i-1) * P.na*P.np + ...
		P.na*P.np*Resource.RcvBuffer(1).numFrames;
	Receive(frameStartIdx+1).callMediaFunc = 1;
	
	% Harmonic pulses
	offset = frameStartIdx;
	for n = 1:P.na*P.np
		[p, a] = ind2sub([P.np P.na], n); % p is fast changing dimension
		Receive(offset+n).bufnum = 2;
		Receive(offset+n).framenum = i;
		Receive(offset+n).aperture = apers(a);
		Receive(offset+n).acqNum = n;
	end
end


%% Specify Process structure array.
% External function definition
EF(1).Function = text2cell('%-beamformRawData');
EF(2).Function = text2cell('%-saveBuffers');

Process(1).classname = 'External';
Process(1).method = 'beamformRawData';
Process(1).Parameters = {'srcbuffer','receive', ... % buffer to process
						 'srcbufnum',1, ...
						 'srcframenum',-1, ... % most recent frame
						 'dstbuffer','none'}; % no output buffer

Process(2).classname = 'External';
Process(2).method = 'saveBuffers';
Process(2).Parameters = {'srcbuffer','receive', ... % buffer to process
						 'srcbufnum',2, ...
						 'srcframenum',0, ... % all frames
						 'dstbuffer','none'}; % no output buffer
					 
%% Specify SeqControl structure arrays.
% Compute the desired acquisition frame rate
fullFrameTime = 1e6/P.acqFPS - (P.acqPRF*P.na*P.np);
% Output the timing parameters
P.acqPRF = 1e6/P.acqPRF;
fprintf('Acquisition frame rate: %d fps.\n', P.acqFPS);
fprintf('Pulse reptition frequency: %.1fkHz.\n', P.acqPRF*1e-3);
if fullFrameTime < 10
	error('Timing for frame is too tight. Try decreasing the acquisition frame rate.')
end

% Timing sequence controls
SeqControl(1).command = 'timeToNextAcq';  % time between synthetic aperture acquisitions
SeqControl(1).argument = P.acqPRF;
SeqControl(2).command = 'timeToNextAcq';
SeqControl(2).argument = fullFrameTime;  % 10000 usec = 10msec time between frames
% Sequence control to return to MATLAB
SeqControl(3).command = 'returnToMatlab';
% Sequence controls to jump
SeqControl(4).command = 'jump';
SeqControl(4).argument = 1;
% nsc is the count of SeqControl objects
nsc = length(SeqControl)+1;

%% Specify Event structure arrays.
n = 1;
for i = 1:Resource.RcvBuffer(1).numFrames
	frameStartIdx = (i-1) * P.na*P.np;
	% Acquire harmonic pulses
	for j = 1:P.np*P.na
		Event(n).info = 'Pulse Inversion TX';
		Event(n).tx = j;
		Event(n).rcv = j + frameStartIdx;
		Event(n).recon = 0;
		Event(n).process = 0;
		Event(n).seqControl = 1; % Acquire at the pulse repetition frequency
		n = n+1;
	end
	% Transfer frame
	Event(n-1).seqControl = [2, nsc]; % Acquire at the acquisition frame rate
	SeqControl(nsc).command = 'transferToHost'; % transfer frame to host buffer
	nsc = nsc+1;
	% Beamform first acquisition of frame
	Event(n).info = 'Beamform';
	Event(n).tx = 0;         % no transmit
	Event(n).rcv = 0;        % no rcv
	Event(n).recon = 0;      % reconstruction
	Event(n).process = 1;    % process
	Event(n).seqControl = 3;
	n = n+1;
end
Event(n).info = 'Jump back to beginning';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0;
Event(n).seqControl = 4;
n = n+1;

% Save data
saveEvent = n;
for i = 1:Resource.RcvBuffer(2).numFrames
	frameStartIdx = (i-1) * P.na*P.np + ...
		P.na*P.np*Resource.RcvBuffer(1).numFrames;
	% Acquire harmonic pulses
	for j = 1:P.np*P.na
		Event(n).info = 'Pulse Inversion TX';
		Event(n).tx = j;
		Event(n).rcv = j + frameStartIdx;
		Event(n).recon = 0;
		Event(n).process = 0;
		Event(n).seqControl = 1; % Acquire at the pulse repetition frequency
		n = n+1;
	end
	% Each image acquisition in the frame is at the acquisition frame rate
	Event(n-1).seqControl = [2, nsc];
	SeqControl(nsc).command = 'transferToHost'; % transfer frame to host buffer
	nsc = nsc+1;
end
Event(n).info = 'Wait for data';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0;
Event(n).seqControl = [2,nsc];
n = n+1;
SeqControl(nsc).command = 'waitForTransferComplete'; % Wait until the transfer is complete
SeqControl(nsc).argument = nsc-1; % Wait until previous transfer is completed.
nsc = nsc+1;

Event(n).info = 'Save raw data';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 2;
Event(n).seqControl = 0;
n = n+1;
Event(n).info = 'Jump back to first event';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0;
Event(n).seqControl = 4;
n = n+1;

%% User specified UI Control Elements
nui = 1;
% - Receive start depth change
UI(nui).Control = {'UserB8','Style','VsSlider','Label','Start Depth',...
	'SliderMinMaxVal',[0,40,P.rxStartMm],'SliderStep',[5 10]/40,'ValueFormat','%3.0f'};
UI(nui).Callback = text2cell('%-RxStartChangeCallback');
nui = nui+1;
% - Receive end depth change
UI(nui).Control = {'UserB7','Style','VsSlider','Label','End Depth',...
	'SliderMinMaxVal',[0,40,P.rxDepthMm],'SliderStep',[5 10]/40,'ValueFormat','%3.0f'};
UI(nui).Callback = text2cell('%-RxDepthChangeCallback');
nui = nui+1;
% - F-Number threshold change
UI(nui).Control = {'UserB6','Style','VsSlider','Label','f/#',...
	'SliderMinMaxVal',[.1,5,R.fnum],'SliderStep',[.1 .5]/4.9,'ValueFormat','%3.2f'};
UI(nui).Callback = text2cell('%-FNumberChangeCallback');
nui = nui+1;
% - Lag threshold change
UI(nui).Control = {'UserB5','Style','VsSlider','Label','Maximum Lag',...
	'SliderMinMaxVal',[1,127,R.maxlag],'SliderStep',[1 5]/126,'ValueFormat','%3.0f'};
UI(nui).Callback = text2cell('%-MaxlagChangeCallback');
nui = nui+1;
% % - Image buffer length
% UI(nui).Control = {'UserB4','Style','VsSlider','Label','Image Buffer Length',...
%                  'SliderMinMaxVal',[100,2000,P.imageBufferSize],'SliderStep',[100,500]/1995,...
% 				 'ValueFormat','%4.0f'};
% UI(nui).Callback = text2cell('%-ImageBufferSizeCallback');
% nui = nui+1;
% - Button to toggle median filter
UI(nui).Control = {'UserB3','Style','VsToggleButton','Label','MedFilt'};
UI(nui).Callback = text2cell('%-MedianFilterCallback');
nui = nui+1;
% Make button half size
UI(nui).Statement = sprintf('set(UI(%d).handle,''Position'', [0.35 0.34 0.15 0.07]);',nui-1);
nui = nui+1;
% - Button to toggle ensemble (0) vs. averaging (1) mode % Hijack C6 UI
UI(nui).Control = {'UserC6','Style','VsToggleButton','Label','Ens. CC'};
UI(nui).Callback = text2cell('%-EnsembleModeCallback');
nui = nui+1;
% Make button half size
UI(nui).Statement = sprintf('set(UI(%d).handle,''Position'', [0.50 0.34 0.15 0.07]);',nui-1);
nui = nui+1;
% % - Button to toggle fundamental (0) vs. harmonic (1) display
% UI(nui).Control = {'UserB2','Style','VsToggleButton','Label','Harmonic'};
% UI(nui).Callback = text2cell('%-HarmonicDisplayCallback');
% nui = nui+1;
% Make button half size
UI(nui).Statement = sprintf('set(UI(%d).handle,''Position'', [0.35 0.24 0.15 0.07]);',nui-1);
nui = nui+1;
% - Button to save data
UI(nui).Control = {'UserC5','Style','VsPushButton','Label','Save'}; % Hijack C5 UI
UI(nui).Callback = text2cell('%-SaveDataCallback');
nui = nui+1;
% Make button half size
UI(nui).Statement = sprintf('set(UI(%d).handle,''Position'', [0.50 0.24 0.15 0.07]);',nui-1);
nui = nui+1;

% - Receive field of view change
UI(nui).Control = {'UserC8','Style','VsSlider','Label','FOV Width',...
	'SliderMinMaxVal',[5,30,R.fovWidthMm],'SliderStep',[5 10]/25,'ValueFormat','%3.0f'};
UI(nui).Callback = text2cell('%-FovWidthChangeCallback');
nui = nui+1;
% - Output samples per wavelength change
UI(nui).Control = {'UserC7','Style','VsSlider','Label','Output SpWl',...
	'SliderMinMaxVal',[0.5,3,R.outspwl],'SliderStep',[.25 .5]/2.5,'ValueFormat','%.2f'};
UI(nui).Callback = text2cell('%-OutSpwlChangeCallback');
nui = nui+1;

% Put B-mode, SLSC dynamic range controls in C5 slot.
cnames = {'Lo','Hi'};
rnames = {'B-mode','SLSC'};
UI(nui).Statement = strjoin(cat(2,text2cell('%-DynamicRangeTable'), ...
	'drt.CellEditCallback = ''', ...
	strrep(strjoin(text2cell('%-DRTCellEditCallback')),'''',''''''),''';'));
nui = nui+1;	

%% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 1;

%% Save all the structures to a .mat file.
curdir = pwd;
sname = [pwd '/MatFiles/' P.configName];
disp(['filename = ''' sname '''; VSX'])
save(sname);
filename = sname;
VSX;

return

%% **** Callback routines to be converted by text2cell function. ****
%-RxStartChangeCallback - Range change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','P.rxStartMm'));
    return
end
P = evalin('base','P');
P.rxStartMm = UIValue;
% Make sure that P.rxStartMm is at least 5mm less than P.rxDepthMm
if P.rxStartMm > P.rxDepthMm-5
	P.rxStartMm = P.rxDepthMm-5;
end
% Make sure that GUI reflects the correct P.rxStartMm
if strcmp(Cntrl,'slider')
	set(h,'String',num2str(P.rxStartMm));
	set(hObject,'Value',P.rxStartMm);
else
	set(hObject,'String',num2str(P.rxStartMm));
	set(h,'Value',P.rxStartMm);
end
% If P.rxStartM has changed
if P.rxStartMm ~= evalin('base','P.rxStartMm')
	Trans = evalin('base','Trans');
	P.rxStart = P.rxStartMm*1e-3 * Trans.frequency*1e6 / 1540;
	Receive = evalin('base', 'Receive');
	for i = 1:size(Receive,2)
		Receive(i).startDepth = P.rxStart;
	end
	Control = evalin('base','Control');
	Control.Command = 'update&Run';
	Control.Parameters = {'PData','Receive','Recon','DisplayWindow','ImageBuffer'};
	clear L12_3v_bmode_synap
	evalin('base','R.initialized = false;');
	assignin('base','P',P);
	assignin('base','Receive',Receive);
	assignin('base','Control', Control);
end
return
%-RxStartChangeCallback

%-RxDepthChangeCallback - Range change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','P.rxDepthMm'));
    return
end
P = evalin('base','P');
P.rxDepthMm = UIValue;
% Make sure that P.rxDepthMm is at least 5mm greater than P.rxStartMm
if P.rxDepthMm < P.rxStartMm+5
	P.rxDepthMm = P.rxStartMm+5;
end
% Make sure that GUI reflects the correct P.rxStartMm
if strcmp(Cntrl,'slider')
	set(h,'String',num2str(P.rxDepthMm));
	set(hObject,'Value',P.rxDepthMm);
else
	set(hObject,'String',num2str(P.rxDepthMm));
	set(h,'Value',P.rxDepthMm);
end
if P.rxDepthMm ~= evalin('base','P.rxDepthMm')
	Trans = evalin('base','Trans');
	P.rxDepth = P.rxDepthMm*1e-3 * Trans.frequency*1e6 / 1540;
	Receive = evalin('base', 'Receive');
	maxAcqLength = sqrt(P.rxDepth^2 + (Trans.numelements*Trans.spacing)^2) - P.rxStart;
	wlsPer128 = 128/(P.spw*2); % wavelengths in 128 samples for 4 samplesPerWave
	for i = 1:size(Receive,2)
		Receive(i).endDepth = P.rxStart + wlsPer128*ceil(maxAcqLength/wlsPer128);
	end
	TGC = evalin('base','TGC');
	TGC.rangeMax = P.rxDepth;
	TGC.Waveform = computeTGCWaveform(TGC);
	Control = evalin('base','Control');
	Control.Command = 'update&Run';
	Control.Parameters = {'PData','Receive','Recon','DisplayWindow','ImageBuffer'};
	clear L12_3v_bmode_synap
	evalin('base','R.initialized = false;')
	assignin('base','P',P);
	assignin('base','Receive',Receive);
	assignin('base','TGC',TGC);
	assignin('base','Control',Control);
end
return
%-RxDepthChangeCallback

%-FovWidthChangeCallback - Field of view width change
R = evalin('base','R');
R.fovWidthMm = UIValue;
if R.fovWidthMm ~= evalin('base','R.fovWidthMm')
	Trans = evalin('base','Trans');
	R.fovWidth = R.fovWidthMm*1e-3 * Trans.frequency*1e6 / 1540;
	clear L12_3v_bmode_synap
	evalin('base','R.initialized = false;')
	assignin('base','R',R);
end
return
%-FovWidthChangeCallback

%-OutSpwlChangeCallback - Pixel density change
R = evalin('base','R');
R.outspwl = UIValue;
if R.outspwl ~= evalin('base','R.outspwl')
	clear L12_3v_bmode_synap
	evalin('base','R.initialized = false;')
	assignin('base','R',R);
end
return
%-OutSpwlChangeCallback

%-SaveDataCallback
% Just launch save script
saveEvent = evalin('base','saveEvent');
str = 'saveEvent';
Control = evalin('base','Control');
Control(1).Command = 'set&Run';
Control(1).Parameters = {'Parameters',1,'startEvent',saveEvent};
evalin('base',['Resource.Parameters.startEvent = ' str ';']);
assignin('base','Control', Control);
return
%-SaveDataCallback

%-DRTCellEditCallback
if any(isnan(drt.Data(:)))||any(drt.Data(2,:) >= drt.Data(1,:)),
	drt.Data = [R.brange(2), R.crange(2); R.brange(1), R.crange(1)];
end;
R.brange(2) = drt.Data(1,1);
R.brange(1) = drt.Data(2,1);
R.crange(2) = drt.Data(1,2);
R.crange(1) = drt.Data(2,2);
assignin('base','R',R);
assignin('base','updatedRange',true);
%-DRTCellEditCallback
%-DynamicRangeTable
R = evalin('base','R');
drt = uitable(findobj('Tag','UI'));
drt.Data = [R.brange(2), R.crange(2); R.brange(1), R.crange(1)];
drt.ColumnWidth = {58,48};
drt.Units = 'normalized';
drt.Position = [0.67 0.43 0.32 0.13];
drt.ColumnName = {'B-mode','SLSC'};
drt.ColumnEditable = [true true];
drt.RowName = {'Hi','Lo'};
drt.ColumnFormat = {'short','bank'};
drt.Position(4) = drt.Extent(4);
%-DynamicRangeTable

%-MedianFilterCallback
R = evalin('base','R');
R.useMedianFilter = UIState;
assignin('base','R',R);
return
%-MedianFilterCallback

% %-ImageBufferSizeCallback - Change length of buffer to acquire
% imBufSz = UIValue;
% if imBufSz ~= evalin('base','P.imageBufferSize')
% 	P = evalin('base','P');
% 	P.imageBufferSize = imBufSz;
% 	assignin('base','P',P);
% 	clear L12_3v_bmode_synap
% 	evalin('base','R.initialized = false;')
% end
% return
% %-ImageBufferSizeCallback

%-EnsembleModeCallback
if UIState ~= evalin('base','R.useEnsembleMode')
	R = evalin('base','R');
	R.useEnsembleMode = UIState;
	assignin('base','R',R);
	L12_3v_bmode_synap(201, ~R.useEnsembleMode);
end
return
%-EnsembleModeCallback

%-MaxlagChangeCallback
if UIValue ~= evalin('base','R.maxlag')
	R = evalin('base','R');
	R.maxlag = UIValue;
	assignin('base','R',R);
	L12_3v_bmode_synap(202, R.maxlag);
end
return
%-MaxlagChangeCallback

%-FNumberChangeCallback - Field of view width change
if UIValue ~= evalin('base','R.fnum')
	R = evalin('base','R');
	R.fnum = UIValue;
	assignin('base','R',R);
	clear L12_3v_bmode_synap
	evalin('base','R.initialized = false;')
end
return
%-FNumberChangeCallback

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% External Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-saveBuffers
saveBuffers(full_buf)

global b_buf c_buf ax lat frameNum fps
if isempty(fps)
	clear fps
end

savedir = evalin('base','savedir');
if ~exist(savedir,'dir'); mkdir(savedir); end
cur = [savedir datestr(now,'yyyymmdd_HHMMSS') '_rawbuf.mat'];

% Get structs
Resource = evalin('base', 'Resource');
Trans	 = evalin('base', 'Trans');
TW		 = evalin('base', 'TW');
TX		 = evalin('base', 'TX');
Receive  = evalin('base', 'Receive');
% Also get variables
P = evalin('base','P');
R = evalin('base','R');
P.TxVoltage = get(findobj(0,'Tag','hv1Value'),'String');

disp(['Saving ' cur '...'])
tic
fid = fopen([cur(1:end-4) '.bin'], 'wb');
full_buf_count = fwrite(fid,full_buf,'int16');
full_buf_size = size(full_buf);
fclose(fid); clear fid;
clear full_buf
save(cur, '-v6', '-regexp','^(?!(full_buf)$).')
fprintf('Took %.3f seconds to save data.\n', toc)
%-saveBuffers

%-beamformRawData
beamformRawData(rcvbuf)

persistent h_bimg h_cimg h_sp1 h_sp2 h_fps nsamps nxmits
persistent t1 t2
persistent b_max c_max bufSize
global b_buf c_buf full_buf ax lat fps frameNum

% On first invocation, initialize
if ~evalin('base','R.initialized')

	% Add path to the CUDA mex file
	vsxBFdir = evalin('base','vsxBFdir');
	addpath([vsxBFdir '/functions'])
%     addpath([vsxBFdir '/../build/vsxBF/L12-3v/bmode'])
	
	% Get user parameters from base workspace
	R = evalin('base','R');
	P = evalin('base','P');
	
	% Grab relevant data structures from base workspace
	Receive	 = evalin('base','Receive');
	TX		 = evalin('base','TX');
	Trans	 = evalin('base','Trans');
	TW		 = evalin('base','TW');
	if ~strcmp(Trans.units, 'wavelengths')
		error('TODO: Provide support for Trans.units == ''mm''...');
	end

	% Parse structures
	nsamps	= Receive(1).endSample;
	spcy    = round(Receive(1).samplesPerWave);
	nxmits	= P.na;
	elemPos = single(Trans.ElementPos(:,1:3));
	beamOri	= [];
	beamDir	= zeros(nxmits, 2, 'single');
	time0wl = zeros(nxmits, 1, 'single');
	apers_0 = zeros(nxmits, 1, 'int32');
	for i = 1:nxmits
		beamDir(i,:) = TX(i*P.np).Steer;
		apers_0(i) = Receive(i*P.np).aperture - 1; % 1-indexed => 0-indexed
		chanIdx = int32(1:128)' + apers_0(i);
		time0wl(i,:) = TW(1).peak + 2*Trans.lensCorrection + ...
			interp1(elemPos(chanIdx,1), TX(i*P.np).Delay, ...
			mean(elemPos(chanIdx,1)), 'spline', 'extrap');
	end
	
	% Define pixel positions. For now, just make them come straight down from xdcr
	rcnOri = single(0:1/R.outspwl:R.fovWidth)';
	% rcnOri = rcnOri(1:end-1);
	rcnOri = rcnOri - mean(rcnOri) + mean(elemPos(chanIdx,1));
	rcnOri = cat(2,rcnOri,0*rcnOri,0*rcnOri + P.rxStart);
	rcnDir = zeros(size(rcnOri,1),2,'single');
	% The samples per wavelength of the TX depends on whether NxFc mode is on.
	if R.useNxFc
		spwl = spcy;
	else
		spwl = 2*spcy;
	end
	nrows   = round((P.rxDepth-P.rxStart) * R.outspwl);
	% Compute delay profiles
	pixPos = computePixelPositions(rcnOri, rcnDir, nrows, R.outspwl);
	pixPos = pixPos(pixPos(:,1,3)>=P.rxStart & pixPos(:,1,3) <= P.rxDepth, :,:);
	[delRx,delTx] = makeDelayTables(elemPos, pixPos, beamOri, beamDir, spwl, time0wl-2*P.rxStart, apers_0);
% 	apoTx = [];
% 	apoRx = [];
% 	apoTx = makeTxApodTable(elemPos, pixPos, [], beamDir, apers_0);
	apoTx = ones(size(pixPos,1), nxmits, size(pixPos,2), 'single');
	apoRx = makeRxApodTable(elemPos, pixPos, R.fnum, apers_0);
	
	delTx = permute(delTx, [1 3 2]);
	delRx = permute(delRx, [1 3 2]);
	apoTx = permute(apoTx, [1 3 2]);
	apoRx = permute(apoRx, [1 3 2]);
% 	cmap = int32(repmat([96:127 0:95]', 1, nxmits));
	cmap = int32(repmat([64:127 0:63]', 1, nxmits));
	% Compute the axis labels
	ax  = pixPos(:,1,3) / (Trans.frequency*1e6) * 1540 * 1000;
	lat = pixPos(1,:,1) / (Trans.frequency*1e6) * 1540 * 1000;
	
	% Initialize
	S = [];
	S.nsamps = nsamps;
	S.npulses = P.np;
	S.nxmits = nxmits;
	S.nelems = 128;
	S.nchans = 128;
	S.nrows = size(pixPos, 1);
	S.ncols = size(pixPos, 2);
	S.outspwl = R.outspwl;
	S.delTx = delTx;
	S.delRx = delRx;
	S.apoTx = apoTx;
	S.apoRx = apoRx;
	S.chanmap = cmap;
	S.sampleMode = Receive.sampleMode;
	S.synthesisMode = 0;
	S.uffFilename = sprintf('%s/../annBF/bmode_network.uff', vsxBFdir);
	
	% Initialize
	clear L12_3v_bmode_synap_tf
	L12_3v_bmode_synap_tf(S);
	
	% Execute GPU BF
	bimg = L12_3v_bmode_synap_tf(rcvbuf);
	bimg = 10*log10(bimg);
	
	% Subtract off max value
	bimg = bimg - max(bimg(:));

	% Initialize cine-loop buffers
	bufSize = 10; % evalin('base','P.imageBufferSize');
	b_buf = zeros(size(bimg,1),size(bimg,2),bufSize,'single');
	b_max = max(bimg(:))*ones(bufSize,1,'single');
	full_buf = zeros(nsamps*nxmits*P.np, 128, bufSize, 'int16');
	
	aspectRatio = (lat(end)-lat(1)) / (ax(end)-ax(1));
	maxHeight = 800;
	maxWidth = 600;
	
	if aspectRatio > maxWidth/maxHeight
		width = maxWidth;
		height = maxWidth/aspectRatio;
	else
		height = maxHeight;
		width = maxHeight*aspectRatio;
	end
			   
	% Plot for the first time
	figure(1001); clf
	set(gcf,'Position',[100 350 width height]);
	colormap gray
	h_sp1 = subplot(121); h_bimg = imagesc(lat, ax, bimg, R.brange); axis image
	set(h_sp1,'Position',[0.05 0.05 0.9 0.9]);
	% set(h_sp1,'DrawMode','fast')
	% set(h_bimg,'EraseMode','none')
	xlabel('Azimuth (mm)')
	ylabel('Depth (mm)')
	title('B-mode')
	colorbar
	
	h_fps = annotation('textbox',[0.45 0.05 0.1 0.05], 'String',sprintf('%2.0f fps',fps), ...
		'FontWeight', 'bold', 'FontSize', 14, 'Color','c','EdgeColor','none',...
		'HorizontalAlignment','center');

	frameNum = 1;
	t1 = tic-5e7;
	
	evalin('base','R.initialized = true;');
else

	P = evalin('base','P');
	R = evalin('base','R');
	
	% Execute GPU BF
	bimg = L12_3v_bmode_synap_tf(rcvbuf);
	bimg = 10*log10(bimg);
	b_max(frameNum) = max(bimg(:));
	
	% Use a history of maximum values to normalize
	% Use fps/2 frames (i.e. half a second)
	maxHistory = mod(frameNum+(-round(fps/10):0)-1, bufSize) + 1;
	bimg = bimg - mean(b_max(maxHistory));

	% Store in buffer
	b_buf(:,:,frameNum) = bimg;
	full_buf(:,:,frameNum) = rcvbuf(1:nsamps*nxmits*P.np, 1:128);
	if R.useMedianFilter
		bimg = medfilt2(bimg,[3 3]);
	end
	
	% Update image
	set(h_bimg, 'CData', bimg);
	if evalin('base', 'updatedRange')
		set(h_sp1, 'CLim', R.brange);
		assignin('base','updatedRange',false);
	end
	
	% Update counters for fps and buffer
	frameNum = mod(frameNum,bufSize)+1;
	
	% Compute fps
	if mod(frameNum,10) == 0
		t2 = toc(t1);
		fps = 10/t2;
		t1 = tic;
		set(h_fps,'String',sprintf(' %.0f fps',fps));
	end
end
%-beamformRawData
