/**
 @file gpuBF/DataArray.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-03

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef DATAARRAY_CUH_
#define DATAARRAY_CUH_

#include "gpuBF.cuh"

namespace rtbf {

/** @brief DataDim is the internal representation of a DataArray.

DataArrays can have an arbitrary number of dimensions. For the purposes of GPU
memory management, we store the data as having the following dimensions:
  x - Length of first dimension (valid samples)
  y - Length of second dimension
  c - Number of channels
  f - Number of frames
  p - Pitch of first dimension in samples (actual size on GPU for alignment)
DataArrays are stored as linear pitched memory, where x is the first
dimension, and the y and c dimensions are stacked.
*/
struct DataDim {
  int x;  ///< Number of elements in the first dimension
  int y;  ///< Number of elements in the second dimension
  int c;  ///< Number of channels
  int f;  ///< Number of frames (e.g., for Doppler imaging)
  int p;  ///< Pitch of first dimension in elements
};
inline bool operator==(DataDim a, DataDim b) {
  return a.x == b.x && a.y == b.y && a.c == b.c && a.f == b.f && a.p == b.p;
}

/** @brief Class for managing data.

This class provides a simple interface for data stored on the GPU, along with
convenient functions for transferring data to and from the CPU. While the array
can have an arbitrary number of dimensions, it will actually be stored as 2D
pitched memory, with all dimensions after the first stacked in memory.

Alternatively, the data can be stored on the CPU by specifying a negative value
for the device ID.

*/
template <typename T>
class DataArray {
 protected:
  // Device information and CUDA objects
  int devID;         ///< Index of the GPU that the data array will reside on
  char devName[32];  ///<Name of GPU that the device array resides on
  char label[32];    ///< String label for data array.
  bool isInit;       ///< Boolean to mark whether the DataArray is initialized.

  // Required data pointers and structures
  T *d_ptr;         ///< A pointer to the output data of the DataArray object
  DataDim dims;     ///< Data dimensions
  size_t typeSize;  ///< Size of one element in bytes
  size_t pitchInBytes;  ///< Pitch of array on GPU

  /// @brief Sets the GPU name for this object
  void setDeviceID(int deviceID) {
    devID = deviceID;
    setDeviceName();
  }
  /// @brief Sets the GPU name for this object
  void setDeviceName() {
    cudaDeviceProp props;
    CCE(cudaGetDeviceProperties(&props, devID));
    strcpy(devName, props.name);
  }

 public:
  /// @brief Constructor
  DataArray();
  /// @brief Destructor
  virtual ~DataArray();

  /// @brief Initialize DataArray directly
  void initialize(int nelemx, int nelemy, int nchans, int nframes = 1,
                  int deviceID = 0, const char *dataLabel = "",
                  int verbosity = 1);
  /// @brief Initialize DataArray using a DataDim
  void initialize(DataDim dimensions, int deviceID = 0,
                  const char *dataLabel = "", int verbosity = 1);
  /// @brief Free pointers
  void reset();

  // Utility functions
  /// @brief Validates the dimensions
  void validateDimensions();
  /// @brief Prints the dimensions
  void printDimensions();

  // Output functions
  /// @brief Returns the GPU index for this object
  int getDeviceID() { return devID; }
  /// @brief Returns the GPU name for this object
  char *getDeviceName() { return devName; }
  /// @brief Returns the device pointer to the data
  T *getDataPtr() { return d_ptr; }
  /// @brief Returns the device pointer to the data
  T *getFramePtr(int frame) { return &d_ptr[dims.p * dims.y * dims.c * frame]; }
  /// @brief Return a pointer to the specified coordinates
  T *getDataPtrCoords(int x, int y, int c, int f = 0) {
    return &d_ptr[x + dims.p * (y + dims.y * (c * dims.c * f))];
  }
  /// @brief Returns the pitch in bytes
  size_t getPitchInBytes() { return pitchInBytes; }
  /// @brief Returns the total size of the array in bytes
  size_t getTotalSizeInBytes() {
    return pitchInBytes * dims.y * dims.c * dims.f;
  }
  /// @brief Returns the total size of the array in elements
  size_t getTotalNumberOfValidElements() {
    return dims.x * dims.y * dims.c * dims.f;
  }
  /// @brief Returns the total size of the actual array in elements
  size_t getTotalNumberOfElements() {
    return dims.p * dims.y * dims.c * dims.f;
  }
  /// @brief Returns whether the DataArray is initialized
  bool isInitialized() { return isInit; }
  /// @brief Returns the number of elements in first dimension
  int getNx() { return dims.x; }
  /// @brief Returns the number of elements in second dimension
  int getNy() { return dims.y; }
  /// @brief Returns the number of channels
  int getNc() { return dims.c; }
  /// @brief Returns the number of frames
  int getNf() { return dims.f; }
  /// @brief Returns the pitch in number of elements
  int getNp() { return dims.p; }
  /// @brief Returns the dimensions as a DataDim
  DataDim getDataDim() { return dims; }

  // Functions to copy data to and from CPU
  /// @brief Copy data to GPU (host pitch in bytes optional)
  void copyToGPU(T *h_ptr, size_t h_pitchInBytes = 0);
  /// @brief Copy data from GPU
  void copyFromGPU(T *h_ptr, size_t h_pitchInBytes = 0);
  /// @brief Copy data to GPU asynchronously
  void copyToGPUAsync(T *h_ptr, cudaStream_t stream, size_t h_pitchInBytes = 0);
  /// @brief Copy data from GPU asynchronously
  void copyFromGPUAsync(T *h_ptr, cudaStream_t stream,
                        size_t h_pitchInBytes = 0);
};

}  // namespace rtbf

#endif /* DATAARRAY_CUH_ */
