/**
 @file gpuBF/ChannelSum.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "ChannelSum.cuh"

namespace rtbf {
namespace kernels = ChannelSumKernels;
template <typename T_in, typename T_out>
ChannelSum<T_in, T_out>::ChannelSum() {
  reset();
}
template <typename T_in, typename T_out>
ChannelSum<T_in, T_out>::~ChannelSum() {
  reset();
}

template <typename T_in, typename T_out>
void ChannelSum<T_in, T_out>::reset() {
  if (this->isInit) {
    this->printMsg("Clearing object.");
  }

  // ChannelSum class members
  in = nullptr;
  nchans_output = 1;

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "ChannelSum");
}

template <typename T_in, typename T_out>
void ChannelSum<T_in, T_out>::initialize(DataArray<T_in> *input,
                                         int nOutputChannels,
                                         cudaStream_t cudaStream,
                                         int verbosity) {
  // If previously initialized, reset
  reset();

  // Read inputs
  in = input;
  nchans_output = nOutputChannels;

  // Device information
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Make a new DataArray for the output
  this->out.initialize(in->getNx(), in->getNy(), nOutputChannels, in->getNf(),
                       this->devID, "Channel Sum", this->verb);

  // Initialize arrays
  this->isInit = true;
}

// Alternative initializer using DataProcessor.
template <typename T_in, typename T_out>
void ChannelSum<T_in, T_out>::initialize(DataProcessor<T_in> *input,
                                         int nOutputChannels,
                                         cudaStream_t cudaStream,
                                         int verbosity) {
  // Pass through to initializer
  initialize(input->getOutputDataArray(), nOutputChannels, cudaStream,
             verbosity);
}

template <typename T_in, typename T_out>
void ChannelSum<T_in, T_out>::sumChannels() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the data dimensions
  DataDim idims = in->getDataDim();
  DataDim odims = this->out.getDataDim();

  dim3 B(256, 1, 1);
  dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
         (odims.c - 1) / B.z + 1);

  // Loop through frames
  CCE(cudaSetDevice(this->devID));
  for (int frame = 0; frame < in->getNf(); frame++) {
    // Get pointers to the data
    T_in *d_in = in->getFramePtr(frame);
    T_out *d_out = this->out.getFramePtr(frame);

    // Execute
    int dsfactor = idims.c / odims.c;
    kernels::sumChannels<<<G, B, 0, this->stream>>>(d_in, idims, d_out, odims,
                                                    dsfactor);
  }
}

template <typename T_in, typename T_out>
void ChannelSum<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(in->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  in = input;
}

template <typename T_in, typename T_out>
__global__ void kernels::sumChannels(T_in *idata, DataDim idims, T_out *odata,
                                     DataDim odims, int ds) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  int c = threadIdx.z + blockIdx.z * blockDim.z;
  if (x < odims.x && y < odims.y && c < odims.c) {
    // Compute output index
    int oidx = x + odims.p * (y + odims.y * c);
    T_out sum;
    sum.x = 0;
    sum.y = 0;
    int stop_elem = (c == odims.c - 1) ? idims.c : (c + 1) * ds;
    for (int elem = c * ds; elem < stop_elem; elem++) {
      if (elem < idims.c) {
        sum += idata[x + idims.p * (y + idims.y * elem)];
      }
    }
    odata[oidx] = sum;
  }
}

// Explicit template specialization instantiation
template class ChannelSum<short2, short2>;
template class ChannelSum<short2, int2>;
template class ChannelSum<short2, float2>;
template class ChannelSum<int2, int2>;
template class ChannelSum<int2, float2>;
template class ChannelSum<float2, float2>;

}  // namespace rtbf
