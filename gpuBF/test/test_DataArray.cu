/**
 @file test/test_DataArray.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-17
*/

#include "../DataArray.cuh"
#include "gtest/gtest.h"

TEST(DataArrayTest, MemcpyFloat) {
  // Create a DataArray
  rtbf::DataDim d;
  d.x = 7;
  d.y = 11;
  d.c = 9;
  d.f = 3;
  rtbf::DataArray<float> A;
  A.initialize(d);
  // Make dummy data
  srand(time(NULL));
  int size = d.x * d.y * d.c * d.f;
  float *h_truth = (float *)malloc(sizeof(float) * size);
  float *h_array = (float *)malloc(sizeof(float) * size);
  for (int i = 0; i < size; i++) {
    h_truth[i] = (float)(rand() % 16) * 1.7f;
  }
  // Copy memory to GPU and back
  A.copyToGPU(h_truth);
  A.copyFromGPU(h_array);
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    float err = (h_truth[i] - h_array[i]);
    sqerr += err * err;
  }
  ASSERT_EQ(sqerr, 0.f);
  A.reset();
  free(h_truth);
  free(h_array);
}

TEST(DataArrayTest, MemcpyFloat2) {
  // Create a DataArray
  rtbf::DataDim d;
  d.x = 7;
  d.y = 11;
  d.c = 9;
  d.f = 3;
  rtbf::DataArray<float2> A;
  A.initialize(d);
  // Make dummy data
  srand(time(NULL));
  int size = d.x * d.y * d.c * d.f;
  float2 *h_truth = (float2 *)malloc(sizeof(float2) * size);
  float2 *h_array = (float2 *)malloc(sizeof(float2) * size);
  for (int i = 0; i < size; i++) {
    h_truth[i].x = (float)(rand() % 16) * 1.7f;
    h_truth[i].y = (float)(rand() % 16) * 9.2f;
  }
  // Copy memory to GPU and back
  A.copyToGPU(h_truth);
  A.copyFromGPU(h_array);
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    float errR = (h_truth[i].x - h_array[i].x);
    float errI = (h_truth[i].y - h_array[i].y);
    sqerr += errR * errR + errI * errI;
  }
  ASSERT_EQ(sqerr, 0.f);
  A.reset();
  free(h_truth);
  free(h_array);
}
