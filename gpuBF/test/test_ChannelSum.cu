/**
 @file test/test_ChannelSum.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-17
*/

#include "../ChannelSum.cuh"
#include "gtest/gtest.h"

namespace rtbf {
namespace ChannelSumTest {
// Use a test fixture to reuse resource between tests
class ChannelSumTest : public ::testing::Test {
 protected:
  // Re-use the same arrays for all tests
  static DataArray<float2> A;
  static float2 *data;
  static DataDim dim;
  // Initialize random data
  static void SetUpTestSuite() {
    // Create a DataArray with dummy data
    dim.x = 17;
    dim.y = 3;
    dim.c = 16;
    dim.f = 2;
    srand(time(NULL));
    int size = dim.x * dim.y * dim.c * dim.f;
    data = (float2 *)malloc(sizeof(float2) * size);
    for (int i = 0; i < size; i++) {
      data[i].x = (float)(rand() % 16) * 1.13f;
      data[i].y = (float)(rand() % 16) * .998f;
    }
    // Copy data to GPU
    A.initialize(dim);
    A.copyToGPU(data);
  }
  // Free memory
  static void TearDownTestSuite() {
    A.reset();
    free(data);
  }
};

DataArray<float2> ChannelSumTest::A;
float2 *ChannelSumTest::data;
DataDim ChannelSumTest::dim;

double compute_sqerr(float2 *a, float2 *b, int size) {
  double sqerr = 0.0;
  for (int i = 0; i < size; i++) {
    double errR = (a[i].x - b[i].x);
    double errI = (a[i].y - b[i].y);
    sqerr += errR * errR + errI * errI;
  }
  return sqerr;
}

void sumChannels_CPU(float2 *in, DataDim d_in, float2 *out, int nchout) {
  memset(out, 0, sizeof(float2) * d_in.x * d_in.y * d_in.f * nchout);
  for (int f = 0; f < d_in.f; f++) {
    for (int ci = 0; ci < d_in.c; ci++) {
      for (int y = 0; y < d_in.y; y++) {
        for (int x = 0; x < d_in.x; x++) {
          int co = ci * nchout / d_in.c;
          if (co >= nchout) co = nchout - 1;
          int oidx = x + d_in.x * (y + d_in.y * (co + nchout * f));
          int iidx = x + d_in.x * (y + d_in.y * (ci + d_in.c * f));
          out[oidx] += in[iidx];
        }
      }
    }
  }
}

// Test summing all channels
TEST_F(ChannelSumTest, SumAllChannels) {
  int nchout = 1;
  // Create a ChannelSum object
  ChannelSum<float2, float2> S;
  S.initialize(&A, nchout, 0, 0);

  // // Sum channels on CPU
  int size = dim.x * dim.y * dim.f * nchout;
  float2 *sumCPU = (float2 *)malloc(sizeof(float2) * size);
  float2 *sumGPU = (float2 *)malloc(sizeof(float2) * size);
  sumChannels_CPU(data, dim, sumCPU, nchout);
  // Sum channels on GPU
  S.sumChannels();
  S.getOutputDataArray()->copyFromGPU(sumGPU);

  // Test for accuracy
  double sqerr = compute_sqerr(sumCPU, sumGPU, size);
  EXPECT_NEAR(sqerr, 0.0, 0.0001);

  S.reset();
  free(sumCPU);
  free(sumGPU);
}

// Test splitting channels into two equal halves
TEST_F(ChannelSumTest, SumHalfChannels) {
  int nchout = 2;
  // Create a ChannelSum object
  ChannelSum<float2, float2> S;
  S.initialize(&A, nchout, 0, 0);

  // // Sum channels on CPU
  int size = dim.x * dim.y * dim.f * nchout;
  float2 *sumCPU = (float2 *)malloc(sizeof(float2) * size);
  float2 *sumGPU = (float2 *)malloc(sizeof(float2) * size);
  sumChannels_CPU(data, dim, sumCPU, nchout);
  // Sum channels on GPU
  S.sumChannels();
  S.getOutputDataArray()->copyFromGPU(sumGPU);

  // Test for accuracy
  double sqerr = compute_sqerr(sumCPU, sumGPU, size);
  EXPECT_NEAR(sqerr, 0.0, 0.0001);

  S.reset();
  free(sumCPU);
  free(sumGPU);
}

// Test splitting channels when input channel is odd
TEST_F(ChannelSumTest, OddChannelCount) {
  // Create a DataArray with dummy data
  DataDim d;
  d.x = 1;
  d.y = 1;
  d.c = 3;
  d.f = 1;
  srand(time(NULL));
  float2 odd[3];
  for (int i = 0; i < 3; i++) {
    odd[i].x = 1.f;
    odd[i].y = 0.f;
  }
  // Copy data to GPU
  DataArray<float2> Aodd;
  Aodd.initialize(d);
  Aodd.copyToGPU(odd);
  int nchout = 2;
  // Sum channels on GPU
  ChannelSum<float2, float2> S;
  S.initialize(&Aodd, nchout, 0, 0);
  float2 res[2];
  S.sumChannels();
  S.getOutputDataArray()->copyFromGPU(res);

  // Test for accuracy
  EXPECT_EQ(res[0].x, 1.f);
  EXPECT_EQ(res[0].y, 0.f);
  EXPECT_EQ(res[1].x, 2.f);
  EXPECT_EQ(res[1].y, 0.f);

  S.reset();
}

}  // namespace ChannelSumTest
}  // namespace rtbf