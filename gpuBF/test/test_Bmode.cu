/**
 @file test/test_Bmode.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-18
*/

#include "../Bmode.cuh"
#include "gtest/gtest.h"

namespace rtbf {
namespace BmodeTest {

TEST(BmodeTest, EnvelopeDetection) {
  // Create a simple 2x2x1 complex-valued array
  float2 data[4];
  srand(time(NULL));
  for (int i = 0; i < 4; i++) {
    data[i].x = (float)(rand() % 16) * 0.14f;
    data[i].y = (float)(rand() % 16) * 14.0f;
  }
  // Make DataArray
  DataArray<float2> A;
  A.initialize(2, 2, 1, 1);
  A.copyToGPU(data);
  // Make Bmode image
  Bmode<float2, float> B;
  B.initialize(&A, 0, 0);
  B.detectEnvelope();
  // Check result
  float resGPU[4];
  B.getOutputDataArray()->copyFromGPU(resGPU);
  for (int i = 0; i < 4; i++) {
    float resCPU = sqrt(data[i].x * data[i].x + data[i].y * data[i].y);
    EXPECT_EQ(resCPU, resGPU[i]);
  }
  B.reset();
  A.reset();
}

TEST(BmodeTest, SpatialCompounding) {
  // Create a simple 2x1x2 complex-valued array
  float2 data[4];
  srand(time(NULL));
  for (int i = 0; i < 4; i++) {
    data[i].x = (float)(rand() % 16) * 0.14f;
    data[i].y = (float)(rand() % 16) * 14.0f;
  }
  // Make DataArray
  DataArray<float2> A;
  A.initialize(2, 1, 2, 1);
  A.copyToGPU(data);
  // Make Bmode image
  Bmode<float2, float> B;
  B.initialize(&A, 0, 0);
  B.detectEnvelope();
  // Check result
  float resGPU[2];
  B.getOutputDataArray()->copyFromGPU(resGPU);
  for (int i = 0; i < 2; i++) {
    int j = i + 2;
    float resCPU1 = sqrt(data[i].x * data[i].x + data[i].y * data[i].y);
    float resCPU2 = sqrt(data[j].x * data[j].x + data[j].y * data[j].y);
    EXPECT_EQ(resCPU1 + resCPU2, resGPU[i]);
  }
  B.reset();
  A.reset();
}

TEST(BmodeTest, LogCompression) {
  // Create a simple 2x2x1 complex-valued array
  float2 data[4];
  srand(time(NULL));
  for (int i = 0; i < 4; i++) {
    data[i].x = (float)(rand() % 16) * 12.1f;
    data[i].y = (float)(rand() % 16) * 14.0f;
  }
  // Make DataArray
  DataArray<float2> A;
  A.initialize(2, 2, 1, 1);
  A.copyToGPU(data);
  // Make Bmode image
  Bmode<float2, float> B;
  B.initialize(&A, 0, 0);
  B.detectEnvelopeLogCompress();
  // Check result
  float resGPU[4];
  B.getOutputDataArray()->copyFromGPU(resGPU);
  for (int i = 0; i < 4; i++) {
    float resCPU = 10 * log10(data[i].x * data[i].x + data[i].y * data[i].y);
    EXPECT_NEAR(resCPU, resGPU[i], 0.0001);
  }
  B.reset();
  A.reset();
}

}  // namespace BmodeTest
}  // namespace rtbf