/**
 @file gpuBF/HilbertTransform.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-07-23

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "HilbertTransform.cuh"

namespace rtbf {
namespace kernels = HilbertTransformKernels;

template <typename T>
HilbertTransform<T>::HilbertTransform() {
  reset();
}
template <typename T>
HilbertTransform<T>::~HilbertTransform() {
  reset();
}

template <typename T>
void HilbertTransform<T>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");
  CCE(cudaSetDevice(this->devID));

  // HilbertTransform class members
  resetFFTDataArray();
  in = nullptr;
  CCE(cudaSetDevice(this->devID));
  if (fftplan != 0) {
    cufftDestroy(fftplan);
    fftplan = 0;
  }

  // Base class members
  // NOTE: this->out is unused because HilbertTransform operates in place.
  this->resetDataProcessor();
  strcpy(this->moniker, "HilbertTransform");
}

template <typename T>
void HilbertTransform<T>::initialize(DataArray<T> *input,
                                     cudaStream_t cudaStream, int verbosity) {
  // If previously initialized, reset
  reset();

  // Set DataProcessor members
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Parse inputs
  in = input;

  // The FFTs must be performed on float2 data. Create one if needed
  initFFTDataArray();

  // Initialize cufftPlan
  int x = fft->getNx();  // Number of rows of valid data
  int p = fft->getNp();  // Number of rows of data pitch
  int y = fft->getNy();  // Number of columns of valid data
  int c = fft->getNc();  // Number of channels in data
  CCE(cudaSetDevice(this->devID));
  CCE(cufftCreate(&fftplan));
  CCE(cufftPlanMany(&fftplan, 1, &x, &p, 1, p, &p, 1, p, CUFFT_C2C, y * c));
  CCE(cufftSetStream(fftplan, this->stream));

  // Mark as initialized
  this->isInit = true;
}

// Generally, we must cast the datatype to float2 for the FFT.
template <typename T>
void HilbertTransform<T>::initFFTDataArray() {
  if (fft->isInitialized()) {
    fft->reset();
  }
  // Make intermediate only 1 frame and re-use for all frames
  fft->initialize(in->getNx(), in->getNy(), in->getNc(), 1, this->devID,
                  "HilbertTransform: Temporary array");
}

// If the input is already float2, we can just use it for the FFT.
template <>
void HilbertTransform<float2>::initFFTDataArray() {
  fft = in;
  this->printMsg("Re-using input DataArray");
  in->printDimensions();
}

// Generally, we must destroy the FFT DataArray
template <typename T>
void HilbertTransform<T>::resetFFTDataArray() {
  fft->reset();
}
template <>
void HilbertTransform<float2>::resetFFTDataArray() {
  // If using in as the FFT DataArray, do not destroy (avoid double free)
  fft = nullptr;
}

template <typename T>
void HilbertTransform<T>::initialize(DataProcessor<T> *input,
                                     cudaStream_t cudaStream, int verbosity) {
  initialize(input->getOutputDataArray(), cudaStream, verbosity);
}

template <typename T>
void HilbertTransform<T>::setInputDataArray(DataArray<T> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(in->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  in = input;
}

// Hilbert transform of in RF data when using a temporary array
template <typename T>
void HilbertTransform<T>::applyHilbertTransform() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  DataDim idim = in->getDataDim();
  DataDim tdim = fft->getDataDim();
  for (int frame = 0; frame < in->getNf(); frame++) {
    T *iptr = in->getDataPtr();
    float2 *tptr = fft->getDataPtr();

    dim3 B(256, 1, 1);
    dim3 G((tdim.p - 1) / B.x + 1, tdim.y, tdim.c);
    // Cast result from type T to type float2 if necessary
    castToFloat2IfNeeded();
    // Apply forward FFT
    CCE(cufftExecC2C(fftplan, tptr, tptr, CUFFT_FORWARD));
    // Make the analytic signal
    if (idim.x % 2 == 0) {
      kernels::makeAnalyticEven<<<G, B, 0, this->stream>>>(tptr, tdim);
    } else {
      kernels::makeAnalyticOdd<<<G, B, 0, this->stream>>>(tptr, tdim);
    }
    // Apply inverse FFT
    CCE(cufftExecC2C(fftplan, tptr, tptr, CUFFT_INVERSE));
    // Cast result from float2 to type T if necessary
    castFromFloat2IfNeeded();
  }
}

// Hilbert transform of in RF data when input and output are already float2
template <>
void HilbertTransform<float2>::applyHilbertTransform() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  DataDim idim = in->getDataDim();
  DataDim tdim = fft->getDataDim();
  for (int frame = 0; frame < in->getNf(); frame++) {
    float2 *iptr = in->getDataPtr();
    float2 *tptr = fft->getDataPtr();

    dim3 B(256, 1, 1);
    dim3 G((tdim.p - 1) / B.x + 1, tdim.y, tdim.c);
    // Apply forward FFT
    CCE(cufftExecC2C(fftplan, tptr, tptr, CUFFT_FORWARD));
    // Make the analytic signal
    if (idim.x % 2 == 0) {
      kernels::makeAnalyticEven<<<G, B, 0, this->stream>>>(tptr, tdim);
    } else {
      kernels::makeAnalyticOdd<<<G, B, 0, this->stream>>>(tptr, tdim);
    }
    // Apply inverse FFT
    CCE(cufftExecC2C(fftplan, tptr, tptr, CUFFT_INVERSE));
  }
}

template <typename T>
void HilbertTransform<T>::castToFloat2IfNeeded() {
  DataDim idim = in->getDataDim();
  DataDim tdim = fft->getDataDim();
  T *iptr = in->getDataPtr();
  float2 *tptr = fft->getDataPtr();
  dim3 B(256, 1, 1);
  dim3 G((tdim.p - 1) / B.x, tdim.y, tdim.c);
  kernels::castToTypeFloat2<<<G, B, 0, this->stream>>>(iptr, idim, tptr,
                                                       tdim.p);
}
template <>
void HilbertTransform<float2>::castToFloat2IfNeeded() {
}  // Do nothing if float2

template <typename T>
void HilbertTransform<T>::castFromFloat2IfNeeded() {
  DataDim idim = in->getDataDim();
  DataDim tdim = fft->getDataDim();
  T *iptr = in->getDataPtr();
  float2 *tptr = fft->getDataPtr();
  dim3 B(256, 1, 1);
  dim3 G((tdim.p - 1) / B.x, tdim.y, tdim.c);
  kernels::castFromTypeFloat2<<<G, B, 0, this->stream>>>(tptr, tdim, iptr,
                                                         idim.p);
}
template <>
void HilbertTransform<float2>::castFromFloat2IfNeeded() {
}  // Do nothing if float2

// Apply weights to first dimension according to MATLAB's Hilbert transform
// The applied weights are slightly different for an odd and even first
// dimension
// length.
__global__ void kernels::makeAnalyticOdd(float2 *data, DataDim dim) {
  int tidx = threadIdx.x + blockIdx.x * blockDim.x;
  int tidy = threadIdx.y + blockIdx.y * blockDim.y;
  int tidz = threadIdx.z + blockIdx.z * blockDim.z;
  int idx = tidx + dim.p * (tidy + dim.y * tidz);
  float weight;

  // Check bounds
  if (tidx < dim.x && tidy < dim.y && tidz < dim.c) {
    if (tidx < 1)
      weight = 1.0f;
    else if (tidx < (dim.x + 1) / 2)
      weight = 2.0f;
    else
      weight = 0.0f;
    data[idx] *= weight / dim.x;  // Apparently CUFFT does not normalize
  }
}
__global__ void kernels::makeAnalyticEven(float2 *data, DataDim dim) {
  int tidx = threadIdx.x + blockIdx.x * blockDim.x;
  int tidy = threadIdx.y + blockIdx.y * blockDim.y;
  int tidz = threadIdx.z + blockIdx.z * blockDim.z;
  int idx = tidx + dim.p * (tidy + dim.y * tidz);
  float weight;

  // Check bounds
  if (tidx < dim.p && tidy < dim.y && tidz < dim.c) {
    if (tidx < 1)
      weight = 1.0f;
    else if (tidx < dim.x / 2)
      weight = 2.0f;
    else if (tidx < dim.x / 2 + 1)
      weight = 1.0f;
    else
      weight = 0.0f;
    data[idx] *= weight / dim.x;  // Apparently CUFFT does not normalize
  }
}

template <typename T>
__global__ void kernels::castToTypeFloat2(T *in, DataDim idim, float2 *out,
                                          int odim_p) {
  int tidx = threadIdx.x + blockIdx.x * blockDim.x;
  int tidy = threadIdx.y + blockIdx.y * blockDim.y;
  int tidz = threadIdx.z + blockIdx.z * blockDim.z;
  if (tidx < idim.x && tidy < idim.y && tidz < idim.c) {
    int iidx = tidx + idim.p * (tidy + idim.y * tidz);
    int oidx = tidx + odim_p * (tidy + idim.y * tidz);
    out[oidx] = loadFloat2(in, iidx);
  }
}
template <typename T>
__global__ void kernels::castFromTypeFloat2(float2 *in, DataDim idim, T *out,
                                            int odim_p) {
  int tidx = threadIdx.x + blockIdx.x * blockDim.x;
  int tidy = threadIdx.y + blockIdx.y * blockDim.y;
  int tidz = threadIdx.z + blockIdx.z * blockDim.z;
  if (tidx < idim.x && tidy < idim.y && tidz < idim.c) {
    int iidx = tidx + idim.p * (tidy + idim.y * tidz);
    int oidx = tidx + odim_p * (tidy + idim.y * tidz);
    saveData2(out, oidx, in[iidx]);  // Cast as T
  }
}

// Real input
template class HilbertTransform<float2>;
template class HilbertTransform<short2>;
template class HilbertTransform<int2>;

}  // namespace rtbf
