/**
 @file gpuBF/Focus.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Focus.cuh"

namespace rtbf {
namespace kernels = FocusKernels;
template <typename T_in, typename T_out>
Focus<T_in, T_out>::Focus() {
  reset();
}
template <typename T_in, typename T_out>
Focus<T_in, T_out>::~Focus() {
  reset();
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");
  CCE(cudaSetDevice(this->devID));

  // Focus members
  memset(&texDesc, 0, sizeof(texDesc));
  memset(&resDesc, 0, sizeof(resDesc));
  raw = nullptr;
  spc = 0.f;
  del.reset();
  apo.reset();
  CCE(cudaDestroyTextureObject(tex));

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "Focus");
}

// Initializer
template <typename T_in, typename T_out>
void Focus<T_in, T_out>::initialize(DataArray<T_in> *input, int nOutputRows,
                                    int nOutputCols, float outputSampsPerWL,
                                    float *h_del, float *h_apo,
                                    float samplesPerCycle,
                                    cudaStream_t cudaStream, int verbosity) {
  // If previously initialized, reset
  reset();

  // Set DataProcessor members
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Parse inputs
  raw = input;
  int nchans = raw->getNc();
  outwlps = 1.f / outputSampsPerWL;

  // Initialize arrays for output, delays, apodization
  CCE(cudaSetDevice(this->devID));
  this->out.initialize(nOutputRows, nOutputCols, nchans, raw->getNf(),
                       this->devID, "Focus");
  del.initialize(this->out.getDataDim(), this->devID, "Focus: Delay table");
  del.copyToGPU(h_del);
  if (h_apo != nullptr) {
    apo.initialize(this->out.getDataDim(), this->devID, "Focus: Apodization");
    apo.copyToGPU(h_apo);
  }

  // Initialize texture object for input
  initTextureObject(raw->getDataPtr());

  // Mark as initialized
  this->isInit = true;
}

// Initializer for DataProcessor input
template <typename T_in, typename T_out>
void Focus<T_in, T_out>::initialize(DataProcessor<T_in> *input, int nOutputRows,
                                    int nOutputCols, float outputSampsPerWL,
                                    float *h_del, float *h_apo,
                                    float samplesPerCycle,
                                    cudaStream_t cudaStream, int verbosity) {
  // Pass through to other initializer
  initialize(input->getOutputDataArray(), nOutputRows, nOutputCols,
             outputSampsPerWL, h_del, h_apo, samplesPerCycle, cudaStream,
             verbosity);
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::initTextureObject(T_in *d_ptr) {
  // Set up textures to use the built-in bilinear interpolation hardware
  // Use texture objects (CC >= 3.0)
  // Texture description
  texDesc.addressMode[0] = cudaAddressModeBorder;
  texDesc.addressMode[1] = cudaAddressModeBorder;
  texDesc.filterMode = cudaFilterModeLinear;
  texDesc.normalizedCoords = 0;
  texDesc.readMode = cudaReadModeElementType;
  // Only if input is 16-bit integer and output is float2
  if (typeid(T_in) == typeid(short2) && typeid(T_out) == typeid(float2))
    texDesc.readMode = cudaReadModeNormalizedFloat;
  // Resource description
  DataDim d = raw->getDataDim();
  resDesc.resType = cudaResourceTypePitch2D;
  resDesc.res.pitch2D.width = d.x;
  resDesc.res.pitch2D.height = d.y * d.c;
  resDesc.res.pitch2D.desc = cudaCreateChannelDesc<T_in>();
  resDesc.res.pitch2D.devPtr = d_ptr;
  resDesc.res.pitch2D.pitchInBytes = raw->getPitchInBytes();
  // Create texture object
  CCE(cudaSetDevice(this->devID));
  CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(raw->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  CCE(cudaSetDevice(this->devID));
  CCE(cudaDestroyTextureObject(tex));
  raw = input;
  initTextureObject(raw->getDataPtr());
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::addGlobalDelay(float globalDelay) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the delay table dimensions
  DataDim dims = del.getDataDim();
  CCE(cudaSetDevice(this->devID));
  dim3 B(256, 1, 1);
  dim3 G((dims.x - 1) / B.x + 1, (dims.y - 1) / B.y + 1,
         (dims.c - 1) / B.z + 1);
  kernels::addGlobalDelay<<<G, B, 0, this->stream>>>(dims, del.getDataPtr(),
                                                     globalDelay);
}

template <typename T_in, typename T_out>
void Focus<T_in, T_out>::focus() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the data dimensions
  DataDim odims = this->out.getDataDim();
  CCE(cudaSetDevice(this->devID));
  // Get normalization factor due to datatype
  float normFactor = 1.f;
  if (typeid(T_in) == typeid(short2) && typeid(T_out) == typeid(float2))
    normFactor = 32767.f;
  dim3 B(16, 4, 4);
  dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
         (odims.c - 1) / B.z + 1);

  // Get CUDA kernel inputs
  float *d_del = del.getDataPtr();
  float *d_apo = apo.getDataPtr();
  int delPitch = del.getNp();

  // Loop through frames
  for (int frame = 0; frame < raw->getNf(); frame++) {
    CCE(cudaSetDevice(this->devID));
    CCE(cudaDestroyTextureObject(tex));
    initTextureObject(raw->getFramePtr(frame));
    T_out *d_out = this->out.getFramePtr(frame);

    // Execute CUDA kernel
    if (spc == 0.f) {          // Modulated data mode
      if (d_apo == nullptr) {  // No apodization
        kernels::focusRF<<<G, B, 0, this->stream>>>(odims, tex, d_del, delPitch,
                                                    d_out, outwlps, normFactor);
      } else {  // With apodization
        kernels::focusRFApod<<<G, B, 0, this->stream>>>(
            odims, tex, d_del, d_apo, delPitch, d_out, outwlps, normFactor);
      }
    } else {  // Baseband data mode
      float cps = 1.f / spc;
      if (d_apo == nullptr) {  // No apodization
        kernels::focusBB<<<G, B, 0, this->stream>>>(
            odims, tex, d_del, delPitch, d_out, outwlps, normFactor, cps);
      } else {  // With apodization
        kernels::focusBBApod<<<G, B, 0, this->stream>>>(
            odims, tex, d_del, d_apo, delPitch, d_out, outwlps, normFactor,
            cps);
      }
    }
    getLastCudaError("Focusing kernel failed.\n");
  }
}

///////////////////////////////////////////////////////////////////////////
// Kernels
///////////////////////////////////////////////////////////////////////////
template <typename T_out>
__global__ void kernels::focusRF(DataDim dims, cudaTextureObject_t tex,
                                 float *d_del, int delPitch, T_out *d_out,
                                 float outwlps, float normFactor) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < dims.x && col < dims.y && chan < dims.c) {
    // Index of the delay table and output focused data
    int didx = row + delPitch * (col + dims.y * chan);
    int oidx = row + dims.p * (col + dims.y * chan);
    // Get receive delay
    float del = d_del[didx];
    // Linearly interpolate at the delay using texture fetching
    float2 data =
        tex2D<float2>(tex, del + 0.5f, col + dims.y * chan + 0.5f) * normFactor;
    // Demodulate data
    float phase = -4.0f * PI * row * outwlps;
    float2 demod = make_float2(cosf(phase), sinf(phase));
    data = complexMultiply(data, demod);
    // Store result in d_out
    saveData2(d_out, oidx, data);
  }
}

template <typename T_out>
__global__ void kernels::focusRFApod(DataDim dims, cudaTextureObject_t tex,
                                     float *d_del, float *d_apo, int delPitch,
                                     T_out *d_out, float outwlps,
                                     float normFactor) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < dims.x && col < dims.y && chan < dims.c) {
    // Index of the delay table and output focused data
    int didx = row + delPitch * (col + dims.y * chan);
    int oidx = row + dims.p * (col + dims.y * chan);
    // Grab apodization
    float apo = d_apo[didx];
    // Only compute if apodization is non-zero
    if (apo != 0.f) {
      // Get receive delay
      float del = d_del[didx];
      // Linearly interpolate at the delay using texture fetching
      float2 data = tex2D<float2>(tex, del + 0.5f, col + dims.y * chan + 0.5f) *
                    normFactor * apo;
      // Get demodulation factor (in radians)
      float phase = -4.0f * PI * row * outwlps;
      float2 demod = make_float2(cosf(phase), sinf(phase));
      // Demodulate data
      data = complexMultiply(data, demod);
      // Store result in d_out
      saveData2(d_out, oidx, data);
    } else {
      // If receive apodization is turned off, skip computation and set to zero.
      saveData2(d_out, oidx, make_float2(0.0f, 0.0f));
    }
  }
}

template <typename T_out>
__global__ void kernels::focusBB(DataDim dims, cudaTextureObject_t tex,
                                 float *d_del, int delPitch, T_out *d_out,
                                 float outwlps, float normFactor,
                                 float cyclesPerSample) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < dims.x && col < dims.y && chan < dims.c) {
    // Index of the delay table and output focused data
    int didx = row + delPitch * (col + dims.y * chan);
    int oidx = row + dims.p * (col + dims.y * chan);
    // Get receive delay
    float del = d_del[didx];
    // Linearly interpolate at the delay using texture fetching
    float2 data =
        tex2D<float2>(tex, del + 0.5f, col + dims.y * chan + 0.5f) * normFactor;
    // Get demodulation factor (in radians)
    float phase = -2.0f * PI * (2 * row * outwlps - del * cyclesPerSample);
    float2 demod = make_float2(cosf(phase), sinf(phase));
    // Apply phase rotation
    data = complexMultiply(data, demod);
    // Store result in d_out
    saveData2(d_out, oidx, data);
  }
}

template <typename T_out>
__global__ void kernels::focusBBApod(DataDim dims, cudaTextureObject_t tex,
                                     float *d_del, float *d_apo, int delPitch,
                                     T_out *d_out, float outwlps,
                                     float normFactor, float cyclesPerSample) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < dims.x && col < dims.y && chan < dims.c) {
    // Index of the delay table and output focused data
    int didx = row + delPitch * (col + dims.y * chan);
    int oidx = row + dims.p * (col + dims.y * chan);
    // Grab apodization
    float apo = d_apo[didx];
    // Only compute if apodization is non-zero
    if (apo != 0.f) {
      // Get receive delay
      float del = d_del[didx];
      // Linearly interpolate at the delay using texture fetching
      float2 data = tex2D<float2>(tex, del + 0.5f, col + dims.y * chan + 0.5f) *
                    normFactor * apo;
      // Get demodulation factor (in radians)
      float phase = -2.0f * PI * (2 * row * outwlps - del * cyclesPerSample);
      float2 demod = make_float2(cosf(phase), sinf(phase));
      // Apply phase rotation
      data = complexMultiply(data, demod);
      // Store result in d_out
      saveData2(d_out, oidx, data);
    } else {
      // If receive apodization is turned off, skip computation and set to zero.
      saveData2(d_out, oidx, make_float2(0.0f, 0.0f));
    }
  }
}

// Kernel to add scalar constant to entire delay table
__global__ void kernels::addGlobalDelay(DataDim dims, float *del,
                                        float globalDelay) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;
  int col = blockIdx.y * blockDim.y + threadIdx.y;
  int page = blockIdx.z * blockDim.z + threadIdx.z;
  if (row < dims.x && col < dims.y && page < dims.c) {
    int idx = row + dims.p * (col + dims.y * page);
    del[idx] += globalDelay;
  }
}

template class Focus<short2, short2>;
template class Focus<short2, float2>;
template class Focus<float2, float2>;
}  // namespace rtbf
