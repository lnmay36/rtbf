/**
 @file gpuBF/FocusSynAp.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "FocusSynAp.cuh"

namespace rtbf {
namespace kernels = FocusSynApKernels;
template <typename T_in, typename T_out>
FocusSynAp<T_in, T_out>::FocusSynAp() {
  reset();
}
template <typename T_in, typename T_out>
FocusSynAp<T_in, T_out>::~FocusSynAp() {
  reset();
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");
  CCE(cudaSetDevice(this->devID));

  // FocusSynAp members
  memset(&texDesc, 0, sizeof(texDesc));
  memset(&resDesc, 0, sizeof(resDesc));
  raw = nullptr;
  nrows = 0;
  ncols = 0;
  nxmits = 0;
  nchans = 0;
  spc = 0.f;
  delTx.reset();
  delRx.reset();
  apoTx.reset();
  apoRx.reset();
  CCE(cudaDestroyTextureObject(tex));

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "FocusSynAp");
}

// Initializer
template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::initialize(
    DataArray<T_in> *input, int nOutputRows, int nOutputCols,
    float outputSampsPerWL, float *h_delTx, float *h_delRx, float *h_apoTx,
    float *h_apoRx, float samplesPerCycle, SynthesisMode synthMode,
    cudaStream_t cudaStream, int verbosity) {
  // If previously initialized, reset
  reset();

  // Set DataProcessor members
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Parse inputs
  raw = input;
  nrows = nOutputRows;
  ncols = nOutputCols;
  mode = synthMode;
  nxmits = raw->getNy();
  nchans = raw->getNc();
  outwlps = 1.f / outputSampsPerWL;
  spc = samplesPerCycle;  // For baseband data
  if (spc > 0.f) this->printMsg("Using baseband input data.", 2);

  // If Tx apodization is provided, Tx delay must be provided as well.
  if (h_apoTx != nullptr && h_delTx == nullptr)
    this->printErr("Specified apodization without delay table.", __FILE__,
                   __LINE__);
  // If one apodization table is provided, the other must be provided as well
  if ((h_apoTx == nullptr) != (h_apoRx == nullptr))
    this->printErr("Only one apodization provided.", __FILE__, __LINE__);

  // Initialize delay and apodization arrays and output
  CCE(cudaSetDevice(this->devID));
  if (mode == SynthesisMode::SynthTx)
    this->out.initialize(nrows, ncols, nchans, raw->getNf(), this->devID,
                         "FocusSynAp");
  else if (mode == SynthesisMode::SynthRx)
    this->out.initialize(nrows, ncols, nxmits, raw->getNf(), this->devID,
                         "FocusSynAp");

  // Initialize transmit and receive delay tables
  delTx.initialize(nrows, ncols, nxmits, 1, this->devID,
                   "FocusSynAp: Tx Delay");
  delRx.initialize(nrows, ncols, nchans, 1, this->devID,
                   "FocusSynAp: Rx Delay");
  delTx.copyToGPU(h_delTx);
  delRx.copyToGPU(h_delRx);

  // If provided, initialize the transmit and receive apodization tables
  if (h_apoTx != nullptr && h_apoRx != nullptr) {
    apoTx.initialize(nrows, ncols, nxmits, 1, this->devID,
                     "FocusSynAp: Tx Apod");
    apoRx.initialize(nrows, ncols, nchans, 1, this->devID,
                     "FocusSynAp: Rx Apod");
    apoTx.copyToGPU(h_apoTx);
    apoRx.copyToGPU(h_apoRx);
  }

  // Initialize texture object for input
  initTextureObject(raw->getDataPtr());

  // Mark as initialized
  this->isInit = true;
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::initialize(
    DataProcessor<T_in> *input, int nOutputRows, int nOutputCols,
    float outputSampsPerWL, float *h_delTx, float *h_delRx, float *h_apoTx,
    float *h_apoRx, float samplesPerCycle, SynthesisMode synthMode,
    cudaStream_t cudaStream, int verbosity) {
  // Pass through to initializer
  initialize(input->getOutputDataArray(), nOutputRows, nOutputCols,
             outputSampsPerWL, h_delTx, h_delRx, h_apoTx, h_apoRx,
             samplesPerCycle, synthMode, cudaStream, verbosity);
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::initTextureObject(T_in *d_ptr) {
  // Set up textures to use the built-in bilinear interpolation hardware
  // Use texture objects (CC >= 3.0)
  // Texture description
  texDesc.addressMode[0] = cudaAddressModeBorder;
  texDesc.addressMode[1] = cudaAddressModeBorder;
  texDesc.filterMode = cudaFilterModeLinear;
  texDesc.normalizedCoords = 0;
  texDesc.readMode = cudaReadModeElementType;
  // Only if input is 16-bit integer and output is float2
  if (typeid(T_in) == typeid(short2) && typeid(T_out) == typeid(float2))
    texDesc.readMode = cudaReadModeNormalizedFloat;
  // Resource description
  DataDim d = raw->getDataDim();
  resDesc.resType = cudaResourceTypePitch2D;
  resDesc.res.pitch2D.width = d.x;
  resDesc.res.pitch2D.height = d.y * d.c;
  resDesc.res.pitch2D.desc = cudaCreateChannelDesc<T_in>();
  resDesc.res.pitch2D.devPtr = d_ptr;
  resDesc.res.pitch2D.pitchInBytes = raw->getPitchInBytes();
  // Create texture object
  CCE(cudaSetDevice(this->devID));
  CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(raw->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  CCE(cudaSetDevice(this->devID));
  CCE(cudaDestroyTextureObject(tex));
  raw = input;
  initTextureObject(raw->getDataPtr());
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::addGlobalDelay(float globalDelay) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the delay table dimensions
  DataDim dims = delRx.getDataDim();
  CCE(cudaSetDevice(this->devID));
  dim3 B(256, 1, 1);
  dim3 G((dims.x - 1) / B.x + 1, (dims.y - 1) / B.y + 1,
         (dims.c - 1) / B.z + 1);
  kernels::addGlobalDelay<<<G, B, 0, this->stream>>>(dims, delRx.getDataPtr(),
                                                     globalDelay);
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::focus() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Set CUDA device
  CCE(cudaSetDevice(this->devID));

  // Get normalization factor according to datatype
  float normFactor = 1.f;
  if (typeid(T_in) == typeid(short2) && typeid(T_out) == typeid(float2))
    normFactor = 32767.f;

  // Get CUDA kernel inputs
  float *d_delTx = delTx.getDataPtr();
  float *d_delRx = delRx.getDataPtr();
  float *d_apoTx = apoTx.getDataPtr();
  float *d_apoRx = apoRx.getDataPtr();
  int delPitch = delTx.getNp();
  int outPitch = this->out.getNp();
  DataDim odims = this->out.getDataDim();

  // Loop through frames
  for (int frame = 0; frame < raw->getNf(); frame++) {
    CCE(cudaSetDevice(this->devID));
    CCE(cudaDestroyTextureObject(tex));
    initTextureObject(raw->getFramePtr(frame));
    T_out *d_out = this->out.getFramePtr(frame);

    dim3 B(16, 4, 4);
    dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
           (odims.c - 1) / B.z + 1);

    // Determine which CUDA kernel to call
    if (spc == 0.f) {                        // Focusing modulated data
      if (mode == SynthesisMode::SynthTx) {  // Synthetic transmit
        if (d_apoTx == nullptr && d_apoRx == nullptr) {  // No apodization
          kernels::focusST_RF<<<G, B, 0, this->stream>>>(
              nrows, ncols, nchans, nxmits, tex, d_delTx, d_delRx, delPitch,
              d_out, outPitch, outwlps, normFactor);
        } else {  // With apodization
          kernels::focusST_RFApod<<<G, B, 0, this->stream>>>(
              nrows, ncols, nchans, nxmits, tex, d_delTx, d_delRx, d_apoTx,
              d_apoRx, delPitch, d_out, outPitch, outwlps, normFactor);
        }
      } else if (mode == SynthesisMode::SynthRx) {  // Synthetic receive
        if (d_apoTx == nullptr && d_apoRx == nullptr) {
          kernels::focusSR_RF<<<G, B, 0, this->stream>>>(
              nrows, ncols, nchans, nxmits, tex, d_delTx, d_delRx, delPitch,
              d_out, outPitch, outwlps, normFactor);
        } else {
          kernels::focusSR_RFApod<<<G, B, 0, this->stream>>>(
              nrows, ncols, nchans, nxmits, tex, d_delTx, d_delRx, d_apoTx,
              d_apoRx, delPitch, d_out, outPitch, outwlps, normFactor);
        }
      }
    } else {                                 // Focusing baseband data
      if (mode == SynthesisMode::SynthTx) {  // Synthetic transmit
        if (d_apoTx == nullptr && d_apoRx == nullptr) {  // No apodization
          kernels::focusST_BB<<<G, B, 0, this->stream>>>(
              nrows, ncols, nchans, nxmits, tex, d_delTx, d_delRx, delPitch,
              d_out, outPitch, outwlps, normFactor, 1.f / spc);
        } else {  // With apodization
          kernels::focusST_BBApod<<<G, B, 0, this->stream>>>(
              nrows, ncols, nchans, nxmits, tex, d_delTx, d_delRx, d_apoTx,
              d_apoRx, delPitch, d_out, outPitch, outwlps, normFactor,
              1.f / spc);
        }
      } else if (mode == SynthesisMode::SynthRx) {       // Synthetic receive
        if (d_apoTx == nullptr && d_apoRx == nullptr) {  // No apodization
          kernels::focusSR_BB<<<G, B, 0, this->stream>>>(
              nrows, ncols, nchans, nxmits, tex, d_delTx, d_delRx, delPitch,
              d_out, outPitch, outwlps, normFactor, 1.f / spc);
        } else {  // With apodization
          kernels::focusSR_BBApod<<<G, B, 0, this->stream>>>(
              nrows, ncols, nchans, nxmits, tex, d_delTx, d_delRx, d_apoTx,
              d_apoRx, delPitch, d_out, outPitch, outwlps, normFactor,
              1.f / spc);
        }
      }
    }
    getLastCudaError("Focusing kernel failed.\n");
  }
}

///////////////////////////////////////////////////////////////////////////
// kernels
///////////////////////////////////////////////////////////////////////////
template <typename T_out>
__global__ void kernels::focusST_RF(int nrows, int ncols, int nchans,
                                    int nxmits, cudaTextureObject_t tex,
                                    float *d_delTx, float *d_delRx,
                                    int delPitch, T_out *d_out, int outPitch,
                                    float outwlps, float normFactor) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < nrows && col < ncols && chan < nchans) {
    // Move all pointers to the current row and column
    d_delTx += row + delPitch * col;
    d_delRx += row + delPitch * col;
    d_out += row + outPitch * col;
    // Get stride of delay tables, output data
    int dstride = delPitch * ncols;
    int ostride = outPitch * ncols;
    // Initialize running sum
    float2 sum = make_float2(0.0f, 0.0f);
    // Get receive delay
    float delR = d_delRx[chan * dstride];
    // Synthesize transmit aperture
    for (int xmit = 0; xmit < nxmits; xmit++) {
      // Linearly interpolate at the delay using texture fetching and sum
      float delRT = delR + d_delTx[xmit * dstride];
      sum += tex2D<float2>(tex, delRT + 0.5f, xmit + nxmits * chan + 0.5f) *
             normFactor;
    }
    // Demodulate data
    float demod = -4.0f * PI * row * outwlps;
    sum = complexMultiply(sum, make_float2(cosf(demod), sinf(demod)));
    // Cast sum to correct datatype and store result in d_out
    saveData2(d_out, chan * ostride, sum);
  }
}

template <typename T_out>
__global__ void kernels::focusST_RFApod(
    int nrows, int ncols, int nchans, int nxmits, cudaTextureObject_t tex,
    float *d_delTx, float *d_delRx, float *d_apoTx, float *d_apoRx,
    int delPitch, T_out *d_out, int outPitch, float outwlps, float normFactor) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < nrows && col < ncols && chan < nchans) {
    // Move all pointers to the current row and column
    d_delTx += row + delPitch * col;
    d_delRx += row + delPitch * col;
    d_apoTx += row + delPitch * col;
    d_apoRx += row + delPitch * col;
    d_out += row + outPitch * col;
    // Get stride of delay tables, output data
    int dstride = delPitch * ncols;
    int ostride = outPitch * ncols;
    // Initialize running sum
    float2 sum = make_float2(0.0f, 0.0f);
    // Only execute if receive apodization is non-zero
    float apoR = d_apoRx[chan * dstride];
    if (apoR != 0.f) {
      // Get receive delay
      float delR = d_delRx[chan * dstride];
      // Synthesize transmit aperture
      for (int xmit = 0; xmit < nxmits; xmit++) {
        // Only execute if transmit apodization is non-zero
        float apoT = d_apoTx[xmit * dstride];
        if (apoT != 0.f) {
          // Linearly interpolate at the delay using texture fetching and sum
          float delRT = delR + d_delTx[xmit * dstride];
          sum += tex2D<float2>(tex, delRT + 0.5f, xmit + nxmits * chan + 0.5f) *
                 normFactor * apoT;
        }
      }
      // Demodulate data
      float demod = -4.0f * PI * row * outwlps;
      sum = complexMultiply(sum, make_float2(cosf(demod), sinf(demod)));
      // Apply receive apodization
      sum *= apoR;
    }
    // Cast sum to correct datatype and store result in d_out
    saveData2(d_out, chan * ostride, sum);
  }
}

template <typename T_out>
__global__ void kernels::focusSR_RF(int nrows, int ncols, int nchans,
                                    int nxmits, cudaTextureObject_t tex,
                                    float *d_delTx, float *d_delRx,
                                    int delPitch, T_out *d_out, int outPitch,
                                    float outwlps, float normFactor) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int xmit = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < nrows && col < ncols && xmit < nxmits) {
    // Move all pointers to the current row and column
    d_delTx += row + delPitch * col;
    d_delRx += row + delPitch * col;
    d_out += row + outPitch * col;
    // Get stride of delay tables, output data
    int dstride = delPitch * ncols;
    int ostride = outPitch * ncols;
    // Initialize running sum
    float2 sum = make_float2(0.0f, 0.0f);
    // Get transmit delay
    float delT = d_delTx[xmit * dstride];
    // Synthesize receive aperture
    for (int chan = 0; chan < nchans; chan++) {
      // Linearly interpolate at the delay using texture fetching and sum
      float delRT = delT + d_delRx[chan * dstride];
      sum += tex2D<float2>(tex, delRT + 0.5f, xmit + nxmits * chan + 0.5f) *
             normFactor;
    }
    // Demodulate data
    float demod = -4.0f * PI * row * outwlps;
    sum = complexMultiply(sum, make_float2(cosf(demod), sinf(demod)));
    // Cast sum to correct datatype and store result in d_out
    saveData2(d_out, xmit * ostride, sum);
  }
}

template <typename T_out>
__global__ void kernels::focusSR_RFApod(
    int nrows, int ncols, int nchans, int nxmits, cudaTextureObject_t tex,
    float *d_delTx, float *d_delRx, float *d_apoTx, float *d_apoRx,
    int delPitch, T_out *d_out, int outPitch, float outwlps, float normFactor) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int xmit = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < nrows && col < ncols && xmit < nxmits) {
    // Move all pointers to the current row and column
    d_delTx += row + delPitch * col;
    d_delRx += row + delPitch * col;
    d_apoTx += row + delPitch * col;
    d_apoRx += row + delPitch * col;
    d_out += row + outPitch * col;
    // Get stride of delay tables, output data
    int dstride = delPitch * ncols;
    int ostride = outPitch * ncols;
    // Initialize running sum
    float2 sum = make_float2(0.0f, 0.0f);
    // Only execute if transmit apodization is non-zero
    float apoT = d_apoTx[xmit * dstride];
    if (apoT != 0.f) {
      // Get transmit delay
      float delT = d_delTx[xmit * dstride];
      // Synthesize transmit aperture
      for (int chan = 0; chan < nchans; chan++) {
        // Only execute if transmit apodization is non-zero
        float apoR = d_apoRx[chan * dstride];
        if (apoR != 0.f) {
          // Linearly interpolate at the delay using texture fetching and sum
          float delRT = delT + d_delRx[chan * dstride];
          sum += tex2D<float2>(tex, delRT + 0.5f, xmit + nxmits * chan + 0.5f) *
                 normFactor * apoR;
        }
      }
      // Demodulate data
      float demod = -4.0f * PI * row * outwlps;
      sum = complexMultiply(sum, make_float2(cosf(demod), sinf(demod)));
      // Apply transmit apodization
      sum *= apoT;
    }
    // Cast sum to correct datatype and store result in d_out
    saveData2(d_out, xmit * ostride, sum);
  }
}

template <typename T_out>
__global__ void kernels::focusST_BB(int nrows, int ncols, int nchans,
                                    int nxmits, cudaTextureObject_t tex,
                                    float *d_delTx, float *d_delRx,
                                    int delPitch, T_out *d_out, int outPitch,
                                    float outwlps, float normFactor,
                                    float cyclesPerSample) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < nrows && col < ncols && chan < nchans) {
    // Move all pointers to the current row and column
    d_delTx += row + delPitch * col;
    d_delRx += row + delPitch * col;
    d_out += row + outPitch * col;
    // Get stride of delay tables, output data
    int dstride = delPitch * ncols;
    int ostride = outPitch * ncols;
    // Initialize running sum
    float2 sum = make_float2(0.0f, 0.0f);
    // Get receive delay
    float delR = d_delRx[chan * dstride];
    // Demodulation factor
    float demod = -4.0f * PI * row * outwlps;
    // Synthesize transmit aperture
    for (int xmit = 0; xmit < nxmits; xmit++) {
      // Linearly interpolate at the delay using texture fetching and sum
      float delRT = delR + d_delTx[xmit * dstride];
      float2 data =
          tex2D<float2>(tex, delRT + 0.5f, xmit + nxmits * chan + 0.5f) *
          normFactor;
      // Also compensate for phase misalignment (in radians)
      float shift = demod + 2.0f * PI * delRT * cyclesPerSample;
      sum += complexMultiply(data, make_float2(cosf(shift), sinf(shift)));
    }
    // Cast sum to correct datatype and store result in d_out
    saveData2(d_out, chan * ostride, sum);
  }
}

template <typename T_out>
__global__ void kernels::focusST_BBApod(
    int nrows, int ncols, int nchans, int nxmits, cudaTextureObject_t tex,
    float *d_delTx, float *d_delRx, float *d_apoTx, float *d_apoRx,
    int delPitch, T_out *d_out, int outPitch, float outwlps, float normFactor,
    float cyclesPerSample) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int chan = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < nrows && col < ncols && chan < nchans) {
    // Move all pointers to the current row and column
    d_delTx += row + delPitch * col;
    d_delRx += row + delPitch * col;
    d_apoTx += row + delPitch * col;
    d_apoRx += row + delPitch * col;
    d_out += row + outPitch * col;
    // Get stride of delay tables, output data
    int dstride = delPitch * ncols;
    int ostride = outPitch * ncols;
    // Initialize running sum
    float2 sum = make_float2(0.0f, 0.0f);
    // Only execute if receive apodization is non-zero
    float apoR = d_apoRx[chan * dstride];
    if (apoR != 0.f) {
      // Get receive delay
      float delR = d_delRx[chan * dstride];
      // Demodulation factor
      float demod = -4.0f * PI * row * outwlps;
      // Synthesize transmit aperture
      for (int xmit = 0; xmit < nxmits; xmit++) {
        // Only execute if transmit apodization is non-zero
        float apoT = d_apoTx[xmit * dstride];
        if (apoT != 0.f) {
          // Linearly interpolate at the delay using texture fetching and sum
          float delRT = delR + d_delTx[xmit * dstride];
          float2 data =
              tex2D<float2>(tex, delRT + 0.5f, xmit + nxmits * chan + 0.5f) *
              normFactor * apoT;
          // Also compensate for phase misalignment (in radians)
          float shift = demod + 2.0f * PI * delRT * cyclesPerSample;
          sum += complexMultiply(data, make_float2(cosf(shift), sinf(shift)));
        }
      }
      // Apply receive apodization
      sum *= apoR;
    }
    // Cast sum to correct datatype and store result in d_out
    saveData2(d_out, chan * ostride, sum);
  }
}

template <typename T_out>
__global__ void kernels::focusSR_BB(int nrows, int ncols, int nchans,
                                    int nxmits, cudaTextureObject_t tex,
                                    float *d_delTx, float *d_delRx,
                                    int delPitch, T_out *d_out, int outPitch,
                                    float outwlps, float normFactor,
                                    float cyclesPerSample) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int xmit = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < nrows && col < ncols && xmit < nxmits) {
    // Move all pointers to the current row and column
    d_delTx += row + delPitch * col;
    d_delRx += row + delPitch * col;
    d_out += row + outPitch * col;
    // Get stride of delay tables, output data
    int dstride = delPitch * ncols;
    int ostride = outPitch * ncols;
    // Initialize running sum
    float2 sum = make_float2(0.0f, 0.0f);
    // Get transmit delay
    float delT = d_delTx[xmit * dstride];
    // Demodulation factor
    float demod = -4.0f * PI * row * outwlps;
    // Synthesize receive aperture
    for (int chan = 0; chan < nchans; chan++) {
      // Linearly interpolate at the delay using texture fetching and sum
      float delRT = delT + d_delRx[chan * dstride];
      float2 data =
          tex2D<float2>(tex, delRT + 0.5f, xmit + nxmits * chan + 0.5f) *
          normFactor;
      // Also compensate for phase misalignment (in radians)
      float shift = demod + 2.0f * PI * delRT * cyclesPerSample;
      sum += complexMultiply(data, make_float2(cosf(shift), sinf(shift)));
    }
    // Cast sum to correct datatype and store result in d_out
    saveData2(d_out, xmit * ostride, sum);
  }
}

template <typename T_out>
__global__ void kernels::focusSR_BBApod(
    int nrows, int ncols, int nchans, int nxmits, cudaTextureObject_t tex,
    float *d_delTx, float *d_delRx, float *d_apoTx, float *d_apoRx,
    int delPitch, T_out *d_out, int outPitch, float outwlps, float normFactor,
    float cyclesPerSample) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;   // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;   // pixel column
  int xmit = blockIdx.z * blockDim.z + threadIdx.z;  // channel
  // Boundary check
  if (row < nrows && col < ncols && xmit < nxmits) {
    // Move all pointers to the current row and column
    d_delTx += row + delPitch * col;
    d_delRx += row + delPitch * col;
    d_apoTx += row + delPitch * col;
    d_apoRx += row + delPitch * col;
    d_out += row + outPitch * col;
    // Get stride of delay tables, output data
    int dstride = delPitch * ncols;
    int ostride = outPitch * ncols;
    // Initialize running sum
    float2 sum = make_float2(0.0f, 0.0f);
    // Only execute if transmit apodization is non-zero
    float apoT = d_apoTx[xmit * dstride];
    if (apoT != 0.f) {
      // Get transmit delay
      float delT = d_delTx[xmit * dstride];
      // Demodulation factor
      float demod = -4.0f * PI * row * outwlps;
      // Synthesize receive aperture
      for (int chan = 0; chan < nchans; chan++) {
        // Only execute if receive apodization is non-zero
        float apoR = d_apoRx[chan * dstride];
        if (apoR != 0.f) {
          // Linearly interpolate at the delay using texture fetching and sum
          float delRT = delT + d_delRx[chan * dstride];
          float2 data =
              tex2D<float2>(tex, delRT + 0.5f, xmit + nxmits * chan + 0.5f) *
              normFactor * apoR;
          // Also compensate for phase misalignment (in radians)
          float shift = demod + 4.0f * PI * delRT * cyclesPerSample;
          sum += complexMultiply(data, make_float2(cosf(shift), sinf(shift)));
        }
      }
      // Apply receive apodization
      sum *= apoT;
    }
    // Cast sum to correct datatype and store result in d_out
    saveData2(d_out, xmit * ostride, sum);
  }
}

// Kernel to add scalar constant to entire delay table
__global__ void kernels::addGlobalDelay(DataDim dims, float *d_del,
                                        float globalDelay) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;
  int col = blockIdx.y * blockDim.y + threadIdx.y;
  int page = blockIdx.z * blockDim.z + threadIdx.z;
  if (row < dims.x && col < dims.y && page < dims.c) {
    int idx = row + dims.p * (col + dims.y * page);
    d_del[idx] += globalDelay;
  }
}
template class FocusSynAp<short2, short2>;
template class FocusSynAp<short2, float2>;
template class FocusSynAp<float2, float2>;
}  // namespace rtbf
