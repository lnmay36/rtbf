/**
 @file gpuBF/FocusSynAp.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-03

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef FOCUSSYNAP_CUH_
#define FOCUSSYNAP_CUH_

#include "DataProcessor.cuh"

namespace rtbf {

// Create an enum to select synthesis mode
enum class SynthesisMode { SynthTx = 0, SynthRx = 1 };

/** @brief Class to apply focusing delays to data. The output is always
demodulated focused channel data.

This class applies focusing delays and optional apodization profiles to the
input data, applying coherent summation to synthesize either the transmit or
receive aperture. The input is of size (nsamps, nxmits, nchans). The output is
of size (nrows, ncols, N), where N depends on the SynthesisMode:
  - SynthTx:  N = nchans
  - SynthRx:  N = nxmits

The input data must be complex, but can be either modulated or baseband.
(NOTE: Baseband has the advantage that the output only has to be Nyquist sampled
with respect to the bandwidth, not the modulation frequency, without loss of
information.)
  - To focus modulated data, specify samplesPerCycle = 0.f.
  - To focus baseband data, specify samplesPerCycle to be with respect to the
    demodulation frequency.

Apodization profiles are optional.
*/
template <typename T_in, typename T_out>
class FocusSynAp : public DataProcessor<T_out> {
 protected:
  // CUDA objects
  cudaTextureObject_t tex;
  cudaTextureDesc texDesc;
  cudaResourceDesc resDesc;
  DataArray<T_in> *raw;  ///< Pointer to input unfocused DataArray

  SynthesisMode mode;      ///< Synthesis mode (e.g., SynthTx)
  int nrows;               ///< Number of rows in output image
  int ncols;               ///< Number of columns in output image
  int nxmits;              ///< Number of transmissions
  int nchans;              ///< Number of channels
  DataArray<float> delTx;  ///< Delay table for transmit
  DataArray<float> delRx;  ///< Delay table for receive
  DataArray<float> apoTx;  ///< Apodization table for transmit
  DataArray<float> apoRx;  ///< Apodization table for receive
  float spc;               ///< Input samples per cycle for demodulated data

  // Output sampling frequency
  float outwlps;  ///< Output wavelengths per sample

  // Internal functions
  void initTextureObject(T_in *d_ptr);

 public:
  // Constructor and destructor
  FocusSynAp();
  virtual ~FocusSynAp();

  // Initialization steps
  /** @brief Function to initialize the object with DataArray input.
  @param input Pointer to input DataArray.
  @param nOutputRows Number of rows in output array.
  @param nOutputCols Number of columns in output array.
  @param outputSampsPerWL Number of samples per wavelength in output image.
  @param h_delTx Pointer to host array of focusing transmit delays.
  @param h_delRx Pointer to host array of focusing receive delays.
  @param h_apoTx Pointer to host array of transmit apodization (optional).
  @param h_apoRx Pointer to host array of receive apodization (optional).
  @param samplesPerCycle Demodulation samples per cycle (baseband mode).
  @param synthMode SynthesisMode. Defaults to SynthTx.
  @param cudaStream cudaStream to use. Defaults to 0.
  @param verbosity Level of verbosity. Defaults to 1.
  */
  void initialize(DataArray<T_in> *input, int nOutputRows, int nOutputCols,
                  float outputSampsPerWL, float *h_delTx, float *h_delRx,
                  float *h_apoTx = nullptr, float *h_apoRx = nullptr,
                  float samplesPerCycle = 0.f,
                  SynthesisMode synthMode = SynthesisMode::SynthTx,
                  cudaStream_t cudaStream = 0, int verbosity = 1);

  /** @brief Function to initialize the object with DataProcessor input.
  @param input Pointer to input DataProcessor.
  @param nOutputRows Number of rows in output array.
  @param nOutputCols Number of columns in output array.
  @param outputSampsPerWL Number of samples per wavelength in output image.
  @param h_delTx Pointer to host array of focusing transmit delays.
  @param h_delRx Pointer to host array of focusing receive delays.
  @param h_apoTx Pointer to host array of transmit apodization (optional).
  @param h_apoRx Pointer to host array of receive apodization (optional).
  @param samplesPerCycle Demodulation samples per cycle (baseband mode).
  @param synthMode SynthesisMode. Defaults to SynthTx.
  @param cudaStream cudaStream to use. Defaults to 0.
  @param verbosity Level of verbosity. Defaults to 1.
  */
  void initialize(DataProcessor<T_in> *input, int nOutputRows, int nOutputCols,
                  float outputSampsPerWL, float *h_delTx, float *h_delRx,
                  float *h_apoTx = nullptr, float *h_apoRx = nullptr,
                  float samplesPerCycle = 0.f,
                  SynthesisMode synthMode = SynthesisMode::SynthTx,
                  cudaStream_t cudaStream = 0, int verbosity = 1);

  /// @brief Add a global delay to the receive delay table
  void addGlobalDelay(float globalDelay);
  /// @brief Set a new input DataArray
  void setInputDataArray(DataArray<T_in> *input);
  /// @brief Set a new input DataArray from a DataProcessor
  void setInputDataProcessor(DataProcessor<T_in> *input) {
    setInputDataArray(input->getOutputDataArray());
  }
  /// @brief Free all dynamically allocated memory.
  void reset();
  /// @brief Apply focusing and apodization to input data
  void focus();
};

namespace FocusSynApKernels {
/// @cond KERNELS
/**	@addtogroup FocusKernels FocusSynAp Kernels
        @{
*/

/** @brief Kernel to focus modulated data with a synthesized transmit aperture
(without apodization).
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param nchans Number of receive channels in the output.
@param nxmits Number of transmits used to perform synthesis
@param tex Texture object of the raw data.
@param d_delTx Transmit delay table.
@param d_delRx Receive delay table.
@param delPitch Pitch of the delay tables (in elements).
@param d_out Device pointer to the output focused data.
@param outPitch Pitch of the output focused data (in elements).
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
*/
template <typename T_out>
__global__ void focusST_RF(int nrows, int ncols, int nchans, int nxmits,
                           cudaTextureObject_t tex, float *d_delTx,
                           float *d_delRx, int delPitch, T_out *d_out,
                           int outPitch, float outwlps, float normFactor);

/** @brief Kernel to focus modulated data with a synthesized transmit aperture
with transmit and receive apodization.
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param nchans Number of receive channels in the output.
@param nxmits Number of transmits used to perform synthesis
@param tex Texture object of the raw data.
@param d_delTx Transmit delay table.
@param d_delRx Receive delay table.
@param d_apoTx Transmit apodization table.
@param d_apoRx Receive apodization table.
@param delPitch Pitch of all delay and apodization tables (in elements).
@param d_out Device pointer to the output focused data.
@param outPitch Pitch of the output focused data (in elements).
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
*/
template <typename T_out>
__global__ void focusST_RFApod(int nrows, int ncols, int nchans, int nxmits,
                               cudaTextureObject_t tex, float *d_delTx,
                               float *d_delRx, float *d_apoTx, float *d_apoRx,
                               int delPitch, T_out *d_out, int outPitch,
                               float outwlps, float normFactor);

/** @brief Kernel to focus modulated data with a synthesized receive aperture
(without apodization).
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param nchans Number of receive channels in the output.
@param nxmits Number of transmits used to perform synthesis
@param tex Texture object of the raw data.
@param d_delTx Transmit delay table.
@param d_delRx Receive delay table.
@param delPitch Pitch of the delay tables (in elements).
@param d_out Device pointer to the output focused data.
@param outPitch Pitch of the output focused data (in elements).
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
*/
template <typename T_out>
__global__ void focusSR_RF(int nrows, int ncols, int nchans, int nxmits,
                           cudaTextureObject_t tex, float *d_delTx,
                           float *d_delRx, int delPitch, T_out *d_out,
                           int outPitch, float outwlps, float normFactor);

/** @brief Kernel to focus modulated data with a synthesized receive aperture
with transmit and receive apodization.
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param nchans Number of receive channels in the output.
@param nxmits Number of transmits used to perform synthesis
@param tex Texture object of the raw data.
@param d_delTx Transmit delay table.
@param d_delRx Receive delay table.
@param d_apoTx Transmit apodization table.
@param d_apoRx Receive apodization table.
@param delPitch Pitch of all delay and apodization tables (in elements).
@param d_out Device pointer to the output focused data.
@param outPitch Pitch of the output focused data (in elements).
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
*/
template <typename T_out>
__global__ void focusSR_RFApod(int nrows, int ncols, int nchans, int nxmits,
                               cudaTextureObject_t tex, float *d_delTx,
                               float *d_delRx, float *d_apoTx, float *d_apoRx,
                               int delPitch, T_out *d_out, int outPitch,
                               float outwlps, float normFactor);

/** @brief Kernel to focus baseband data with a synthesized transmit aperture
(without apodization).
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param nchans Number of receive channels in the output.
@param nxmits Number of transmits used to perform synthesis
@param tex Texture object of the raw data.
@param d_delTx Transmit delay table.
@param d_delRx Receive delay table.
@param delPitch Pitch of the delay tables (in elements).
@param d_out Device pointer to the output focused data.
@param outPitch Pitch of the output focused data (in elements).
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
*/
template <typename T_out>
__global__ void focusST_BB(int nrows, int ncols, int nchans, int nxmits,
                           cudaTextureObject_t tex, float *d_delTx,
                           float *d_delRx, int delPitch, T_out *d_out,
                           int outPitch, float outwlps, float normFactor,
                           float cyclesPerSample);

/** @brief Kernel to focus baseband data with a synthesized transmit aperture
with transmit and receive apodization.
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param nchans Number of receive channels in the output.
@param nxmits Number of transmits used to perform synthesis
@param tex Texture object of the raw data.
@param d_delTx Transmit delay table.
@param d_delRx Receive delay table.
@param d_apoTx Transmit apodization table.
@param d_apoRx Receive apodization table.
@param delPitch Pitch of all delay and apodization tables (in elements).
@param d_out Device pointer to the output focused data.
@param outPitch Pitch of the output focused data (in elements).
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
*/
template <typename T_out>
__global__ void focusST_BBApod(int nrows, int ncols, int nchans, int nxmits,
                               cudaTextureObject_t tex, float *d_delTx,
                               float *d_delRx, float *d_apoTx, float *d_apoRx,
                               int delPitch, T_out *d_out, int outPitch,
                               float outwlps, float normFactor,
                               float cyclesPerSample);

/** @brief Kernel to focus baseband data with a synthesized receive aperture
(without apodization).
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param nchans Number of receive channels in the output.
@param nxmits Number of transmits used to perform synthesis
@param tex Texture object of the raw data.
@param d_delTx Transmit delay table.
@param d_delRx Receive delay table.
@param delPitch Pitch of the delay tables (in elements).
@param d_out Device pointer to the output focused data.
@param outPitch Pitch of the output focused data (in elements).
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
*/
template <typename T_out>
__global__ void focusSR_BB(int nrows, int ncols, int nchans, int nxmits,
                           cudaTextureObject_t tex, float *d_delTx,
                           float *d_delRx, int delPitch, T_out *d_out,
                           int outPitch, float outwlps, float normFactor,
                           float cyclesPerSample);

/** @brief Kernel to focus baseband data with a synthesized receive aperture
with transmit and receive apodization.
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param nchans Number of receive channels in the output.
@param nxmits Number of transmits used to perform synthesis
@param tex Texture object of the raw data.
@param d_delTx Transmit delay table.
@param d_delRx Receive delay table.
@param d_apoTx Transmit apodization table.
@param d_apoRx Receive apodization table.
@param delPitch Pitch of all delay and apodization tables (in elements).
@param d_out Device pointer to the output focused data.
@param outPitch Pitch of the output focused data (in elements).
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param normFactor Normalization factor for float vs. int texture fetching.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
*/
template <typename T_out>
__global__ void focusSR_BBApod(int nrows, int ncols, int nchans, int nxmits,
                               cudaTextureObject_t tex, float *d_delTx,
                               float *d_delRx, float *d_apoTx, float *d_apoRx,
                               int delPitch, T_out *d_out, int outPitch,
                               float outwlps, float normFactor,
                               float cyclesPerSample);

/** @brief Kernel to add a single global delay to a delay table. Useful for when
different imaging configurations have different time zero values.
@param nrows Number of rows in the output (excluding pitch).
@param ncols Number of columns in the output.
@param npages Size of third dimension of delay table
@param d_del Delay table.
@param delPitch Pitch of the delay table (in elements).
@param globalDelay Scalar constant to add to delay table.
*/
__global__ void addGlobalDelay(DataDim dims, float *d_del, float globalDelay);
/** @}*/
/// @endcond

}  // namespace FocusSynApKernels
}  // namespace rtbf

#endif /* FOCUSSYNAP_CUH_ */
