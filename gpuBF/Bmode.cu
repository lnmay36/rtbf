/**
 @file gpuBF/Bmode.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-13

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Bmode.cuh"

namespace rtbf {
namespace kernels = BmodeKernels;
template <typename T_in, typename T_out>
Bmode<T_in, T_out>::Bmode() {
  reset();
}
template <typename T_in, typename T_out>
Bmode<T_in, T_out>::~Bmode() {
  reset();
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");

  // Bmode class members
  in = nullptr;

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "Bmode");
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::initialize(DataArray<T_in> *input,
                                    cudaStream_t cudaStream, int verbosity) {
  // If previously initialized, reset
  reset();

  // Read inputs
  in = input;

  // Device information
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Make a new DataArray for the output
  this->out.initialize(in->getNx(), in->getNy(), 1, in->getNf(), this->devID,
                       "B-mode image", this->verb);

  // Initialize arrays
  this->isInit = true;
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::initialize(DataProcessor<T_in> *input,
                                    cudaStream_t cudaStream, int verbosity) {
  // Pass through to initializer
  initialize(input->getOutputDataArray(), cudaStream, verbosity);
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(in->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  in = input;
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::detectEnvelope() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the data dimensions
  DataDim idims = in->getDataDim();
  DataDim odims = this->out.getDataDim();

  dim3 B(256, 1, 1);
  dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
         (odims.c - 1) / B.z + 1);

  // Loop through frames
  for (int frame = 0; frame < in->getNf(); frame++) {
    // Get pointers to the data
    T_in *d_in = in->getFramePtr(frame);
    T_out *d_out = this->out.getFramePtr(frame);

    // Execute
    kernels::envDetect<<<G, B, 0, this->stream>>>(d_in, idims, d_out, odims);
  }
}

template <typename T_in, typename T_out>
void Bmode<T_in, T_out>::detectEnvelopeLogCompress() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the data dimensions
  DataDim idims = in->getDataDim();
  DataDim odims = this->out.getDataDim();

  dim3 B(256, 1, 1);
  dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
         (odims.c - 1) / B.z + 1);

  // Loop through frames
  for (int frame = 0; frame < in->getNf(); frame++) {
    // Get pointers to the data
    T_in *d_in = in->getFramePtr(frame);
    T_out *d_out = this->out.getFramePtr(frame);

    // Execute
    kernels::envDetectLogComp<<<G, B, 0, this->stream>>>(d_in, idims, d_out,
                                                         odims);
  }
}

///////////////////////////////////////////////////////////////////////////
// Kernels
///////////////////////////////////////////////////////////////////////////
template <typename T_in, typename T_out>
__global__ void kernels::envDetect(T_in *idata, DataDim idims, T_out *odata,
                                   DataDim odims) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  if (x < odims.x && y < odims.y) {
    // Compute output index
    int oidx = x + odims.p * y;
    // Initialize running sum
    T_out sum = (T_out)0.f;
    // Loop through any images to compound
    for (int c = 0; c < idims.c; c++) {
      int iidx = x + idims.p * (y + idims.y * c);
      T_in tmp = idata[iidx];
      sum += sqrtf(tmp.x * tmp.x + tmp.y * tmp.y);
    }
    // Write to output
    odata[oidx] = sum;
  }
}

template <typename T_in, typename T_out>
__global__ void kernels::envDetectLogComp(T_in *idata, DataDim idims,
                                          T_out *odata, DataDim odims) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  if (x < odims.x && y < odims.y) {
    // Compute output index
    int oidx = x + odims.p * y;
    // Initialize running sum
    T_out sum = (T_out)0.f;
    // Loop through any images to compound
    for (int c = 0; c < idims.c; c++) {
      int iidx = x + idims.p * (y + idims.y * c);
      T_in tmp = idata[iidx];
      sum += sqrtf(tmp.x * tmp.x + tmp.y * tmp.y);
    }
    // Write to output
    odata[oidx] = 20 * log10f(sum);
  }
}

// Explicit template specialization instantiation
template class Bmode<short2, float>;
template class Bmode<float2, float>;

}  // namespace rtbf
